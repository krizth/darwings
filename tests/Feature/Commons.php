<?php


namespace Tests\Feature;


use App\Models\User;


use Illuminate\Support\Collection;
use Illuminate\Testing\TestResponse;

trait Commons
{

    protected  function commonsSetup(){
        $oauth_client_id = 2;
        $oauth_client = "7glWMMkz8T8IcWVFWt5Cb1I3LRmTN1bnNDfvjY80";
        $headers=['Accept' => 'application/json',"Authorization" =>"Bearer ". User::find(2)->createToken('token')->accessToken];
        $baseUrl ="/api";
        $body = [
            'username' => 'krizth2307@gmail.com',
            'password' => 'password',
            'client_id' => $oauth_client_id,
            'client_secret' => $oauth_client,
            'grant_type' => 'password',
            'scope' => '*'
        ];
     return new Collection(["headers"=>$headers, "baseUrl"=>$baseUrl,"body"=>$body]);
    }
    protected  function commonAssertions(TestResponse $req){
        $req->assertJsonPath('status','OK');
        $req->assertJsonPath('code',200);
    }
}
