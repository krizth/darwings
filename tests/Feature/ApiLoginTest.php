<?php

namespace Tests\Feature;

use Tests\TestCase;

class ApiLoginTest extends TestCase
{

    use Commons;

    public function testOauthLogin()
    {
        $setup=$this->commonsSetup();

        $resp = $this->json('POST', $setup["baseUrl"].'/oauth/token', $setup["body"], $setup["headers"]);
        $resp->assertStatus(200)
            ->assertJsonStructure(['token_type', 'expires_in', 'access_token', 'refresh_token']);

    }
}
