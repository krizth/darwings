<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CajaControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use Commons;
    public function testCajaControllerMethodGet(){
        $setup=$this->commonsSetup();
        $req=$this->json('GET',$setup["baseUrl"].'/caja',$setup["body"], $setup["headers"],);
        //$this->commonAssertions($req);
        $req->assertStatus(500);

      /*  $req=$this->json('GET',$setup["baseUrl"].'/caja/create',$setup["body"], $setup["headers"]);
        $req->assertStatus(200);
        $this->commonAssertions($req);*/
    }

    public function testCajaControllerMethodPatch(){
        $setup=$this->commonsSetup();
        $req=$this->json('PATCH',$setup["baseUrl"].'/caja',$setup["body"], $setup["headers"]);
        $req->assertStatus(405);
    }
    public function testCajaControllerMethodDelete(){
        $setup=$this->commonsSetup();
        $req=$this->json('DELETE',$setup["baseUrl"].'/caja',$setup["body"], $setup["headers"]);
        $req->assertStatus(405);
    }
    public function testCajaControllerMethodCurrency(){
        $setup=$this->commonsSetup();
        $req=$this->json('GET',$setup["baseUrl"].'/caja/currency',$setup["body"], $setup["headers"]);
        $req->assertStatus(500);
    }
}
