<?php

namespace Tests\Feature;

use Tests\TestCase;

class AuthCodesTest extends TestCase
{
    use Commons;

    public function testAuthCodesControllerMethodGet(){
        $setup=$this->commonsSetup();
        $req=$this->json('GET',$setup["baseUrl"].'/auth-code',$setup["body"], $setup["headers"],);
        $this->commonAssertions($req);
        $req->assertStatus(200);

        $req=$this->json('GET',$setup["baseUrl"].'/auth-code/create',$setup["body"], $setup["headers"]);
        $req->assertStatus(200);
        $this->commonAssertions($req);
        $req->assertJsonPath('content.id_owner', 2);
    }

    public function testAuthCodesControllerMethodPatch(){
        $setup=$this->commonsSetup();
        $req=$this->json('PATCH',$setup["baseUrl"].'/auth-code',$setup["body"], $setup["headers"]);
        $req->assertStatus(405);
    }
    public function testAuthCodesControllerMethodDelete(){
        $setup=$this->commonsSetup();
        $req=$this->json('DELETE',$setup["baseUrl"].'/auth-code',$setup["body"], $setup["headers"]);
        $req->assertStatus(405);
    }
}
