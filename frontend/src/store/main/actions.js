import {Loading,Notify, LocalStorage, QSpinnerIos, date} from 'quasar'
import Vue from 'vue';
export async function login({dispatch}, payload) {
  try {
    const login = await Vue.prototype.$axios.request({
      baseURL: Vue.prototype.$auth.baseURL,
      url: "oauth/token",
      method: "post",
      headers:{'Accept': 'application/json'},
      auth: {
        username: Vue.prototype.$auth.id,
        password: Vue.prototype.$auth.secret,
      },
      data: {
        grant_type: Vue.prototype.$auth.grant_type,
        username: payload.username,
        password: payload.password
      }
    });
    if (login.data.access_token !== undefined) {
      LocalStorage.set("access_token", `Bearer ${login.data.access_token}`);
      this.$router.push({name:'orders.index'});
    } else {
      Notify.create({
        message: 'Autentificacion fallida',
        position: 'center',
        color: 'danger',
        timeout: 1000
      });
    }
  } catch (e) {
    console.log(e.message)
    Notify.create({
      message: 'Autentificacion fallida',
      position: 'center',
      color: 'danger',
      timeout: 1000
    });
  }
}
export async function Authxios({},payload) {

  let url =payload.url===undefined?this.$router.app._route.fullPath:payload.url,
    method = payload.method===undefined?'get':payload.method,
    data = payload.data===undefined?{}:payload.data,
    showLoading = payload.showLoading===undefined?true:payload.showLoading,
    message = payload.message===undefined?'Cargando Datos':payload.message,
    showErrors=payload.showErrors===undefined?false:payload.showErrors,
    headers=payload.headers===undefined?{}:payload.headers;
    console.log("payload",payload);
  if (showLoading) {
    Loading.show({
      spinner: QSpinnerIos,
      message: message,

    });
  }
    let config = {
      baseURL: Vue.prototype.$auth.baseURL,
      url: url,
      method: method,
      ...headers,
      headers: {
        'Accept': 'application/json',
        'Authorization': LocalStorage.getItem("access_token"),
      },
      data: {...data}
    };
    if (method.toLowerCase() === 'get') {
      delete config.data;
      config = {...config, params:{...data}}
    }
    const resp = await Vue.prototype.$axios.request(config);
    if (showLoading) {
      Loading.hide();
    }
    return resp;

}
export function showForm({commit},payload){
  commit('MainForm',{payload:{mode:payload.mode,...payload.data},show:true})
}
export function configSession({dispatch}){
return dispatch('Authxios',{
  url:Vue.prototype.$auth.baseURL+'/login/options',
  showLoading:false,
  method:'get'});

}
export async function printOrder({dispatch},payload){
  const req= await dispatch('Authxios',{
    url:Vue.prototype.$auth.baseURL+'/orders/print/'+payload.id_pedido,
    showLoading:true,
    method:'get'});
    const style=`* {
      font-size: 15px;
    font-family: 'Verdana';
  }

  td,
    th,
    tr,
    table {
    border-top: 1px solid black;
    border-collapse: collapse;
  }

  td.description,
    th.description {
    width: 30px;
    max-width: 30px;
  }

  td.quantity,
    th.quantity {
    width: 40px;
    max-width: 40px;
    word-break: break-all;
  }

  td.price,
    th.price {
    width: 40px;
    max-width: 40px;
    word-break: break-all;
    font-size: 10px;
  }
  td.desc,
    th.desc {
   width: 40px;
    max-width: 40px;
    word-break: break-all;
  }

.centered {
    text-align: center;
    align-content: center;
  }
.title {
    font-size: 30px;
    font-family: 'Allura', cursive;
  }
.ticket {
   width: 70mm;
    max-width: 70mm;
  }

  img {
    max-width: inherit;
    width: inherit;
  }

@media print {
  .hidden-print,
  .hidden-print * {
      display: none !important;
  }
  }`
    let orderDetail=req.data.detalle_pedidos.map((det)=>{
      return `<tr>
        <td className="quantity">${det.cantidad}</td>
        <td className="description">${det.productos.nombre}</td>
        <td className="price">${'$'+det.productos.precio}</td>

      </tr>
      <tr >
      <td colspan="3">Comentario: <br/>${det.descripcion!==null?det.descripcion:''}</td>
        </tr>
`
    }).join('');
  const WinPrint = window.open('', 'print', 'left=50%,top=50%,width=500,height=600,toolbar=0,scrollbars=0,status=0');
 let medio=[];
  req.data.medio_pago.map((e)=>{
    medio.push(e.nombre)
  })
  const html=`<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Allura&family=Genos:wght@100;400&family=Montserrat+Alternates:ital,wght@1,600&display=swap" rel="stylesheet">
        <style>
        ${style}
        </style>
        <title>Recibo de pago N° ${req.data.id}</title>
    </head>
    <body>
        <div class="ticket">
        <p class="centered" ><a class="title">Darwings</a></p>
            <p class="centered">

                <br> Comanda N° ${req.data.id}
                <br>Cliente: ${req.data.cliente.nombre}
                <br>Direccion: ${req.data.direccion}
                <br>Teléfono: ${req.data.telefono}
                <br>${req.data.sucursal.nombre}
                </p>
                <table>
                <th class="quantity">Cant.</th>
                        <th class="description">Desc.</th>
                        <th class="price">$</th>
                <tbody>
                 ${orderDetail}
                  <tr>
                        <th class="quantity"></th>
                        <th class="description">Sub Total</th>
                        <th class="price">${'$'+req.data.subtotal}</th>
                          <th class="desc"></th>
                    </tr>
                  <tr>
                        <th class="quantity"></th>
                        <th class="description">Total</th>
                        <th class="price">${'$'+req.data.total}</th>
                        <th class="desc"></th>
                  </tr>
                </tbody>
            </table>
               <p class="centered">Medio de pago: ${medio.join(',')}</p>
                <p class="centered">Comentarios: ${req.data.comentario}</p>
               <p class="centered">Delivery: ${'$'+req.data.precio_delivery}</p>
            <p class="centered">${date.formatDate(req.data.created_at,'DD/MM/YYYY hh:mm')}</p>
            <p class="centered">Gracias por Preferirnos!</p>
        </div>
    </body>
</html>`;
  WinPrint.document.write(html);
  WinPrint.document.close();
  WinPrint.focus();
  setTimeout(()=>{
    WinPrint.print();
  },1000)

}
export function logout({dispatch}){
    dispatch('Authxios',{
      showLoading:true,
      url:'logout',
      method:'post'}).then(()=>{
      LocalStorage.clear();
      this.$router.push({name:'login.index'});
    });

}
export function isLogued({dispatch}){
  const req= dispatch('Authxios',{
    showLoading:false,
    url:Vue.prototype.$auth.baseURL+'/login/verify',
    method:'get'});
  req.catch((err)=>{
    if(err.response.status===401){
      LocalStorage.clear();
      this.$router.push({name:'login.index'});
    }
  });
}
