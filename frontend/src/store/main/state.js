export default function () {
  return {
    mapOrderDetail:{
      origin:[],
      processed:[]
    },
    orderTotal:0,
    exchange:0,
    MainForm:{
      show:false,
      payload:{}
    },
    MainTable:{
      reload:false
    },
    authCodesForm:{
      show:false,
      payload:{
        motivo_movimiento:{
          id:0,
          motivo:""
        },
        method:()=>{},
        data:undefined
      }
    }
  }
}
