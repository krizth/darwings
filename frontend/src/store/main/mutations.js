export function MainForm (state,payload) {
  state.MainForm=payload;
}
export function ReloadMainTable(state,payload){
  state.MainTable.reload=payload;
}
export function mapOrderDetail(state,payload){
state.mapOrderDetail.origin=payload;
}
export function mapOrderDetailProcessed(state,payload){
  state.mapOrderDetail.processed=payload;
}
export function mapExchange(state,payload){
  state.exchange=payload;
}
export function orderTotal(state,payload){
  state.orderTotal=payload;
}
export function AuthCodesForm(state,payload){
  state.authCodesForm.payload=payload;
}
export function ShowAuthCodesForm(state,payload){
  state.authCodesForm.show=payload;
}
export function SetAuthCode(state,payload){
  state.authCodesForm.payload.auth_code=payload;
}
export function AuthCodesReset(state,payload){
  state.authCodesForm={
    show:false,
    payload:{
      motivo_movimiento:{
        id:0,
        motivo:""
      },
      method:()=>{},
      data:undefined
    }
  };
}



