import {LocalStorage} from "quasar";

var total=0;
export function reloadMainTable(state){
  console.log("state reload main",state.MainTable.reload)
  return state.MainTable.reload
}
export function mapOrderDetail(state){
  total=0;
  if(Array.isArray(state.mapOrderDetail.origin)){
    let output=state.mapOrderDetail.origin.map((el)=> {
      total+=el.producto.precio*el.cantidad;
      return "Producto: "+el.producto.nombre+"\n Precio:"+el.producto.precio+"\n Cantidad:"+el.cantidad;
    });
    return [...output,"\n Total: "+total];
  }

}
export function mapExchange(state){

  if(typeof parseInt(state.exchange)==='number'&&total>0){
    const withdrawl=parseInt(state.exchange)-total;
    return "Cambio: "+withdrawl;
  }else{
    if(parseInt(state.exchange)<0){
      return 'no se admiten valores negativos'
    }
    return '';
  }
}
export function logued(){
  return LocalStorage.getItem('access_token')!==null;
}
export function isConfiguredSession(){
 return LocalStorage.getItem('sucursal')!==null&&
  (LocalStorage.getItem('caja')!==null||LocalStorage.getItem('isAdmin')!==null);
}
