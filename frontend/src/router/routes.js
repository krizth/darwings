
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: 'home', component: () => import('pages/home.vue') }

    ],
    meta:{
      guest:false,
      requiresAuth:true
    }
  },
  {
    path: '/loginConfig',
    component: () => import('layouts/Authentification.vue'),
    children: [
      { path: '',name:'login.config', component: () => import('pages/loginParams.vue') }
    ],
    meta:{
      guest:false,
      requiresAuth:true
    }
  },
  {
    path: '/login',
    component: () => import('layouts/Authentification.vue'),
    children: [
      { path: '',name:'login.index', component: () => import('pages/login.vue') }
    ]
    ,
    meta:{
      guest:true,
      requiresAuth:false
    }
  },
  {
    path: '/users',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {path: '',name:"user.index", component: () => import('pages/user.vue')}
    ],
    meta:{
      guest:true,
      requiresAuth:true
    }
  },
  {
    path: '/auth-code',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {path: '',name:"code.index", component: () => import('pages/auth_codes.vue')}
    ],
    meta:{
      guest:true,
      requiresAuth:true
    }
  },
  {
    path: '/products',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {path: '',name:"products.index", component: () => import('pages/products.vue')}
    ],
    meta:{
      guest:false,
      requiresAuth:true
    }
  },
  {
    path: '/clients',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {path: '',name:"clients.index", component: () => import('pages/clients.vue')}
    ],
    meta:{
      guest:false,
      requiresAuth:true
    }
  },
  {
    path: '/orders',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {path: '',name:"orders.index", component: () => import('pages/orders.vue')}
    ],
    meta:{
      guest:false,
      requiresAuth:true
    }
  },
  {
    path: '/stockProduct',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {path: '',name:"stockProduct.index", component: () => import('pages/stock_preparaciones.vue')}
    ],
    meta:{
      guest:false,
      requiresAuth:true
    }
  },

  {
    path: '/caja',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {path: '',name:"caja.index", component: () => import('pages/caja.vue')}
    ],
    meta:{
      guest:false,
      requiresAuth:true
    }
  },
  {
    path: '/discount',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {path: '',name:"discount.index", component: () => import('pages/discount.vue')}
    ],
    meta:{
      guest:false,
      requiresAuth:true
    }
  },
  {
    path: '/delivery',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {path: '',name:"delivery.index", component: () => import('pages/delivery.vue')}
    ],
    meta:{
      guest:false,
      requiresAuth:true
    }
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
