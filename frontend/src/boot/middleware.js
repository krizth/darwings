// import something here

// "async" is optional;
// more info on params: https://quasar.dev/quasar-cli/boot-files
export default async ( { urlPath, redirect } ) => {
  if(urlPath==='/users'){
    redirect({ name: 'user.index' });
    return;
  }
}
