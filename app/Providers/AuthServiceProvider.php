<?php

namespace App\Providers;

use App\Models\Caja;
use App\Models\DetallePedido;
use App\Models\Movimiento;
use App\Models\Pedido;
use App\Models\User;
use App\Policies\CajaPolicy;
use App\Policies\DetallePedidoPolicy;
use App\Policies\MovimientoPolicy;
use App\Policies\PedidoPolicy;
use App\Policies\UserPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Movimiento::class=>MovimientoPolicy::class,
        Caja::class=>CajaPolicy::class,
        DetallePedido::class=>DetallePedidoPolicy::class,
        Pedido::class=>PedidoPolicy::class,
        User::class=>UserPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Passport::routes(null,['prefix'=>'api/oauth']);
    }
}
