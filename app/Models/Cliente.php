<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property string $nombre
 * @property string $rut
 * @property string $direccion
 * @property boolean $amigos_de_darwings
 * @property integer $puntos
 * @property string $ciudad
 * @property integer $pedidos
 * @property mixed $location
 * @property string $created_at
 * @property string $updated_at
 * @property Pedido[] $pedido
 */
class Cliente extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cliente';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'rut', 'direccion','telefono', 'amigos_de_darwings', 'puntos', 'ciudad', 'pedidos', 'location', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pedidos()
    {
        return $this->hasMany('App\Models\Pedido', 'id_cliente');
    }
    public static function puntosDarwings($Cliente,$puntos){

        if($Cliente->amigos_de_darwings){
            $Cliente->puntos+=$puntos;
            $Cliente->save();
        }
        if((!is_null($Cliente->rut))&&(!$Cliente->amigo_de_darwings)){
            $Cliente->pedidos=Pedido::where([['id_cliente','=',$Cliente->id],['pagado','=',true]])->count();
            $Cliente->save();
        }
    }

    public static function updatePoints($Encabezado,$negative=true){
        $Cliente=Cliente::find($Encabezado->id_cliente);
            if($Encabezado->pagado){
                $detalle_pedido=DetallePedido::where('id_pedido',$Encabezado->id)->with('producto')->get();
                foreach ($detalle_pedido as $item) {
                    Cliente::puntosDarwings($Cliente,$negative?-($item->producto["puntos"]*$item->cantidad):($item->producto["puntos"]*$item->cantidad));
                }
            }

    }
}
