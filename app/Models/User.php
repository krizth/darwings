<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    /**
     * @property integer $id
     * @property string $name
     * @property string $email
     * @property string $email_verified_at
     * @property string $password
     * @property boolean $isAdmin
     * @property mixed $workspace
     * @property string $remember_token
     * @property string $created_at
     * @property string $updated_at
     * @property CajaUsuario[] $cajaUsuarios
     * @property Movimiento[] $movimientos
     * @property SucursalesUsuario[] $sucursalesUsuarios
     */
    use HasApiTokens, Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps =false;
    protected $fillable = ['name','isAdmin','workspace', 'email', 'email_verified_at', 'password', 'remember_token', 'created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'workspace'=>'array',
        'isAdmin'=>'boolean'
    ];
    public static function isAdmin(){
       $user= User::find(Auth::id());
        return $user->isAdmin;
    }

    public static function workspace($key){
        $user=User::find(Auth::id());
        return $user->workspace[$key];
    }
    public function cajaUsuarios()
    {
        return $this->hasMany('App\Models\CajaUsuario', 'id_users');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function movimientos()
    {
        return $this->hasMany('App\Models\Movimiento', 'id_usuario');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sucursalesUsuarios()
    {
        return $this->hasMany('App\Models\SucursalesUsuario', 'id_usuario');
    }
}
