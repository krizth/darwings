<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $id_pedido
 * @property integer $id_producto
 * @property integer $cantidad
 * @property string $descripcion
 * @property string $created_at
 * @property string $updated_at
 * @property Pedido $pedido
 * @property Producto $producto
 */
class DetallePedido extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'detalle_pedido';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['id_pedido', 'id_producto', 'cantidad', 'descripcion', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pedido()
    {
        return $this->belongsTo('App\Models\Pedido', 'id_pedido');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function productos()
    {
        return $this->belongsTo('App\Models\Productos', 'id_producto')->withTrashed();
    }
    public function producto()
    {
        return $this->belongsTo('App\Models\Productos', 'id_producto')->withTrashed();
    }
}
