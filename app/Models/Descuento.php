<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $porcentaje
 * @property string $descripcion
 * @property boolean $activo
 * @property Producto[] $productos
 */
class Descuento extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'descuento';
    public $timestamps= false;
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['porcentaje', 'descripcion'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productos()
    {
        return $this->hasMany('App\Models\Producto', 'id_descuento');
    }
}
