<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * @property integer $id
 * @property integer $id_owner
 * @property integer $id_user_destino
 * @property integer $id_user_uso
 * @property integer $id_motivo_movimiento
 * @property integer $codigo
 * @property integer $cantidad
 * @property string $imagen
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property MotivoMovimiento $motivoMovimiento
 * @property User $user
 * @property User $owner
 * @property User $userUse
 */
class AuthCodes extends Model
{
    use SoftDeletes;
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['id_owner', 'id_user_destino', 'id_user_uso', 'id_motivo_movimiento', 'codigo', 'cantidad', 'imagen', 'comentario' , 'deleted_at', 'created_at', 'updated_at'];

    /**
     * @return bool
     */
    public static function handleImages($isBase64, $filename)
    {
        if(empty($filename)){
            return "";
        }
        if (!$isBase64) {
            $path = storage_path('app/images/' . $filename);
            if (!File::exists($path)) {
                abort(404);
            }
            $file = File::get($path);
            $type = File::mimeType($path);
            $response = \Response::make($file, 200);
            $response->header("Content-Type", $type);
            return $response;
        } else {
            $path = storage_path('app/' . $filename);
            $type = File::mimeType($path);
            return 'data:' . $type . ';base64,' . base64_encode(file_get_contents($path));
        }
    }

    public static function requestAuthCode($request){
         return ['require_authcode' => true, 'data' => $request->all(), 'motivo_movimiento' => MotivoMovimiento::select('motivo', 'id', 'operador')->where('req_autorizacion', true)->whereNotIn('id', [MotivoMovimiento::$APERTURA, MotivoMovimiento::$CIERRE])->get()];
    }
    public static function useAuthCode($code=[],$cantidad = null)
    {
        if (!is_array($code)) return false;
        if (sizeof($code) == 0) return false;
        $fields = ['id_user_uso' => Auth::id(), 'updated_at' => Carbon::now(), 'comentario' => $code["comentario"]];
        if ($code["imagen"] !== null) $fields['imagen'] = $code["imagen"];
        if ($code["motivo_movimiento"] !== null) $fields['id_motivo_movimiento'] = $code["motivo_movimiento"]["id"];
        if ($cantidad !== null) {
            $fields['cantidad'] = $cantidad;
        }else{
            $fields['cantidad']=$code['cantidad'];
        }
        DB::beginTransaction();
        try{
            if(AuthCodes::where([
                ['codigo',"=",$code["codigo"]],
                ['id_user_uso',"=",null]
                ])->update($fields)===1){
                DB::commit();
                return true;
            }else{
                DB::rollBack();
                return false;
            }
        }catch (\Exception $e){
            DB::rollBack();
            return false;
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function motivoMovimiento()
    {
        return $this->belongsTo('App\Models\MotivoMovimiento', 'id_motivo_movimiento');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo('App\Models\User', 'id_owner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function targetUser()
    {
        return $this->belongsTo('App\Models\User', 'id_user_destino');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userUse()
    {
        return $this->belongsTo('App\Models\User', 'id_user_uso');
    }
}
