<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id_caja
 * @property integer $id_users
 * @property Caja $caja
 * @property User $user
 */
class CajaUsuario extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    public $timestamps=false;
    protected $table = 'caja_usuario';

    /**
     * @var array
     */
    protected $fillable = ['id_caja', 'id_users'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function caja()
    {
        return $this->hasOne('App\Models\Caja', 'id','id_caja');
    }

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'id_users');
    }
}
