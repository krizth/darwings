<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * @property integer $id
 * @property integer $id_producto
 * @property integer $id_sucursal
 * @property integer $cantidad
 * @property string $created_at
 * @property string $updated_at
 * @property Producto $producto
 * @property Sucursal $sucursal
 */
class StockProductos extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['id_producto', 'id_sucursal', 'cantidad', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producto()
    {
        return $this->belongsTo('App\Models\Productos', 'id_producto');
    }

    /**
     * @param $OrderID
     */

    public static function updateStock($ProductID, $dif)
    {

        DB::beginTransaction();

        $stock = StockProductos::where([
            ['id_producto', "=", $ProductID],
            ['id_sucursal', "=", User::workspace("sucursal")["id"]]
        ])->first();
        if (!$stock) {
            throw new \Exception("No se encontro el stock ");
        }
        $stock->cantidad += $dif;
        if ($stock->cantidad < 0) {
            throw new \Exception("No hay stock del producto: " . $stock->producto->nombre);
        }
        $stock->save();
        DB::commit();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sucursal()
    {
        return $this->belongsTo('App\Models\Sucursal', 'id_sucursal');
    }
}
