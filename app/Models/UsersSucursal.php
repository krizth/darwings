<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id_sucursal
 * @property integer $id_users
 * @property Sucursal $sucursal
 * @property User $user
 */
class UsersSucursal extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'users_sucursal';

    /**
     * @var array
     */
    protected $fillable = ['id_sucursal', 'id_users'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sucursal()
    {
        return $this->belongsTo('App\Models\Sucursal', 'id_sucursal');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'id_users');
    }
}
