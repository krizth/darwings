<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property integer $cantidad
 * @property string $descripcion
 * @property string $created_at
 * @property string $updated_at
 * @property CajaSucursal[] $cajaSucursals
 * @property CierreCaja[] $cierreCajas
 * @property Movimiento[] $movimientos
 */

class Caja extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'caja';
    protected $casts=["estado"=>'boolean'];
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['id_sucursal','cantidad','estado','id_caja','id_apertura','id_ultimo_cierre', 'descripcion', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */


    public function sucursal()
    {
        return $this->hasOne('App\Models\Sucursal', 'id','id_sucursal');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cierreCajas()
    {
        return $this->hasMany('App\Models\CierreCaja', 'id_caja');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function movimientos()
    {
        return $this->hasMany('App\Models\Movimiento', 'id_caja');
    }
    /**
     * @return array
     */
    public static function createRule(): array
    {
        return [
            'cantidad'=>'required|numeric|min:0',
            'estado'=>'required',
            'descripcion'=>'required|min:3',
            'sucursal'=>'required'
        ];
    }
    public static function cuadrarCaja($IdCaja){
        $caja=Caja::find($IdCaja);
        $cantidad=Movimiento::
            whereNotIn('id_motivo_movimiento',[MotivoMovimiento::$APERTURA,MotivoMovimiento::$CIERRE])
            ->where([['id_caja',"=",$IdCaja]])
            ->sum('movimiento.monto');
       $caja->cantidad=$cantidad;
       $caja->save();
    }
}
