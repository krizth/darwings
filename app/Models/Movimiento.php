<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property integer $id_caja
 * @property integer $id_apertura
 * @property integer $id_medio_pago
 * @property integer $id_usuario
 * @property integer $id_pedido
 * @property integer $id_motivo_movimiento
 * @property integer $monto
 * @property string $created_at
 * @property string $updated_at
 * @property Movimiento $movimiento
 * @property Caja $caja
 * @property MedioPago $medioPago
 * @property MotivoMovimiento $motivoMovimiento
 * @property Pedido $pedido
 * @property User $user
 */
class Movimiento extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'movimiento';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['id_caja', 'id_apertura', 'id_cierre','id_medio_pago', 'id_usuario', 'id_pedido',
        'id_motivo_movimiento', 'monto', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function apertura()
    {
        return $this->belongsTo('App\Models\Movimiento', 'id_apertura');
    }

    public function cierre()
    {
        return $this->belongsTo('App\Models\Movimiento', 'id_cierre');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function caja()
    {
        return $this->belongsTo('App\Models\Caja', 'id_caja');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function medioPago()
    {
        return $this->belongsTo('App\Models\MedioPago', 'id_medio_pago');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function motivoMovimiento()
    {
        return $this->belongsTo('App\Models\MotivoMovimiento', 'id_motivo_movimiento');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pedido()
    {
        return $this->belongsTo('App\Models\Pedido', 'id_pedido');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'id_usuario');
    }
}
