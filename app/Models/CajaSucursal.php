<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id_sucursal
 * @property integer $id_caja
 * @property string $created_at
 * @property string $updated_at
 * @property Caja $caja
 * @property Sucursal $sucursal
 */
class CajaSucursal extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'caja_sucursal';

    /**
     * @var array
     */
    protected $fillable = ['id_sucursal', 'id_caja', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function caja()
    {
        return $this->belongsTo('App\Models\Caja', 'id_caja');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sucursal()
    {
        return $this->belongsTo('App\Models\Sucursal', 'id_sucursal');
    }
}
