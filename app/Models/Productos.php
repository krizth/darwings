<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property integer $id_descuento
 * @property string $nombre
 * @property string $descripcion
 * @property integer $precio
 * @property integer $puntos
 * @property string $created_at
 * @property string $updated_at
 * @property Descuento $descuento
 * @property DetallePedido[] $detallePedidos
 * @property StockProducto[] $stockProductos
 */
class Productos extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['id_descuento','puntos', 'nombre', 'descripcion', 'precio', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function descuento()
    {
        return $this->belongsTo('App\Models\Descuento', 'id_descuento');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function detallePedidos()
    {
        return $this->hasMany('App\Models\DetallePedido', 'id_producto')->withTrashed();;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function stockProductos()
    {
        return $this->hasMany('App\Models\StockProducto', 'id_producto')->withTrashed();;
    }
}
