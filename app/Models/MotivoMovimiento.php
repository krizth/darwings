<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property string $motivo
 * @property string $operador
 * @property boolean $contable
 * @property Movimiento[] $movimientos
 */
class MotivoMovimiento extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    public static $VENTA=1;
    public static $ENTRADA=2;
    public static $VUELTO=3;
    public static $AJUSTE=4;
    public static $APERTURA=5;
    public static $CIERRE=6;
    public static $CANCELACION=7;
    public static $DESCUENTO=8;
    public static $DELIVERY=9;
    public static $GASTO=10;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'motivo_movimiento';
    protected $casts=["req_autorizacion"=>'boolean'];
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['req_autorizacion','motivo', 'operador', 'contable'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function movimientos()
    {
        return $this->hasMany('App\Models\Movimiento', 'id_motivo_movimiento');
    }
}
