<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $nombre
 * @property mixed $ubicacion
 * @property string $direccion
 * @property string $ciudad
 * @property string $telefono
 * @property CajaSucursal[] $cajaSucursals
 * @property CierreCaja[] $cierreCajas
 * @property Pedido[] $pedidos
 * @property SucursalesUsuario[] $sucursalesUsuarios
 * @property User[] $users
 */
class Sucursal extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sucursal';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'ubicacion', 'direccion', 'ciudad', 'telefono'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cajaSucursals()
    {
        return $this->hasMany('App\Models\CajaSucursal', 'id_sucursal');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pedidos()
    {
        return $this->hasMany('App\Models\Pedido', 'id_sucursal');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sucursalesUsuarios()
    {
        return $this->hasMany('App\Models\SucursalesUsuario', 'id_sucursal');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'users_sucursal', 'id_sucursal', 'id_users');
    }
}
