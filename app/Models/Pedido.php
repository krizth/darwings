<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

/**
 * @property integer $id
 * @property integer $id_cliente
 * @property integer $id_estado_pedido
 * @property integer $id_sucursal
 * @property integer $id_delivery
 * @property string $direccion
 * @property string $telefono
 * @property string $comentario
 * @property integer $total
 * @property string $created_at
 * @property string $updated_at
 * @property Cliente $cliente
 * @property Delivery $delivery
 * @property EstadoPedido $estadoPedido
 * @property Sucursal $sucursal
 * @property DetallePedido[] $detallePedidos
 */
class Pedido extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $casts=['pagado'=>'boolean','medio_pago'=>'array'];
    protected $table = 'pedido';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = [ 'subtotal','pago' , 'descuento' ,'id_cliente','precio_delivery','pagado', 'medio_pago','id_estado_pedido', 'id_sucursal', 'id_delivery', 'direccion', 'telefono', 'comentario', 'total', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente', 'id_cliente');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function delivery()
    {
        return $this->belongsTo('App\Models\Delivery', 'id_delivery');
    }

    public static function totalUpdater($detalle_pedido,$Encabezado,$precio_delivery = 0,$updateStock=true){
        $descuento_producto=0;
      try{
          DB::beginTransaction();

          if($detalle_pedido){
                  foreach ($detalle_pedido as $pedido) {
                      DetallePedido::create([
                          'id_pedido' => $Encabezado->id,
                          'id_producto' => $pedido["producto"]["id"],
                          'cantidad' => $pedido["cantidad"],
                          'descripcion' => $pedido["descripcion"]?:""
                      ]);
                  }
          }else {
              $detalle_pedido=DetallePedido::with('producto')->where('id_pedido',$Encabezado->id)->get();
              if(!$detalle_pedido)throw new \Exception("Ocurrio un error registrando el encabezado del pedido");
          }
            $subtotal=0;
            $descuento_encabezado=0;
          foreach ($detalle_pedido as $pedido){
              if($pedido["cantidad"]>0){
                  if($updateStock) StockProductos::updateStock($pedido["producto"]["id"],-$pedido["cantidad"]);
                  $producto = Productos::find($pedido["producto"]["id"]);
                  if(!$producto)throw  new \Exception("No se encontro el producto, o se ha eliminado");
                  $subtotal += (($producto->precio) * $pedido["cantidad"]);
                  $descuento=Descuento::find($producto->id_descuento);
                  if($descuento)$descuento_encabezado += round($producto->precio * ($descuento->porcentaje/100),0,PHP_ROUND_HALF_UP) ;
              }
          }

          $Encabezado->subtotal=$subtotal;
          $Encabezado->descuento=$descuento_encabezado;

          $Encabezado->total = $Encabezado->subtotal - $descuento_encabezado;
          $Encabezado->total+=$precio_delivery;
          $Encabezado->save();
          DB::commit();
          return $Encabezado->total;
      }catch (\Exception $ex){
          DB::rollBack();
         abort(500,$ex->getMessage());
      }
    }

    public static function pagarPedido($OrderID,$idCaja,$pagado,$isAuthorized=null,$Discount=0){

            $pedido=Pedido::find($OrderID);
            $apertura=Caja::find($idCaja);
            if($pedido->pagado){
                $cantidad_antigua= Movimiento::where('id_pedido',$pedido->id)->sum('monto');
                $caja=Caja::find(User::workspace("caja")["id"]);
                $caja->cantidad-=$cantidad_antigua;
                $caja->save();

            }

            Movimiento::where('id_pedido',$OrderID)->delete();
            //$delivery=$pedido->precio_delivery;
            if(!$pedido)throw new \Exception("Ocurrio un error al pagar el pedido N°".$OrderID);
            if(is_null($isAuthorized)||!$isAuthorized)if($pedido->total>$pagado)throw new \Exception("El monto ingresado no alcanza para pagar el pedido, total:".$pedido->total.", pagado:".$pagado);
            $cantidad_en_medio_pago=0;

            DB::beginTransaction();
           foreach ($pedido->medio_pago as $medio){
               Movimiento::create([
                   'id_caja'=>$idCaja,
                   'id_medio_pago'=>$medio["id"],
                   'id_apertura'=>$apertura->id_apertura,
                   "id_usuario"=>Auth::id(),
                   "id_pedido"=>$pedido->id,
                   'id_motivo_movimiento'=>MotivoMovimiento::$VENTA,
                   'monto'=>sizeof($pedido->medio_pago)>1?$medio["cantidad"]:$pagado
               ]);
               $cantidad_en_medio_pago+=sizeof($pedido->medio_pago)>1?$medio["cantidad"]:$pagado;
           }

            $vuelto=($pedido->total-$pagado);
            if($vuelto<0){
                $caja=Caja::find(User::workspace("caja")["id"]);
                if($caja){
                    if($caja->cantidad>($vuelto*-1)){
                        Movimiento::create([
                            'id_caja'=>$idCaja,
                            "id_usuario"=>Auth::id(),
                            'id_apertura'=>$apertura->id_apertura,
                            "id_pedido"=>$pedido->id,
                            'id_motivo_movimiento'=>MotivoMovimiento::$VUELTO,
                            'monto'=>$vuelto
                        ]);
                    }else{
                        throw new \Exception("No se ha realizado el pago, no hay efectivo sufuciente en la caja, contacte al administrador");
                    }
                }else{
                    throw new \Exception("No se ha seleccionado una caja para este movimiento");
                }
            }
            $caja=Caja::find($idCaja);

            $caja->cantidad+=$pedido->total;
            if($cantidad_en_medio_pago>$pagado) throw new \Exception("Ocurrio un error al pagar el pedido N°".$OrderID.", Cantidad en medio de pago:".$cantidad_en_medio_pago.', pagado:'.$pagado);
            if(!is_null($isAuthorized)||$isAuthorized) {
                $pedido->descuento=$Discount;
                Movimiento::create([
                    'id_caja'=>$idCaja,
                    'id_medio_pago'=>$pedido->id_medio_pago,
                    'id_apertura'=>$apertura->id_apertura,
                    "id_usuario"=>Auth::id(),
                    "id_pedido"=>$pedido->id,
                    'id_motivo_movimiento'=>MotivoMovimiento::$DESCUENTO,
                    'monto'=>$Discount
                ]);
                $caja->cantidad+=$Discount;
            }
            $pedido->pagado=true;
            $caja->save();
            $pedido->save();
            DB::commit();


    }

    public static function cancelarPedido($OrderID,$deleteOrder=true){
        DB::beginTransaction();
        $pedido=Pedido::find($OrderID);
        $descuento_a_caja=0;
        $detalle_pedido=DetallePedido::where('id_pedido',$OrderID)->get();
        $caja=Caja::find(User::workspace("caja")["id"]);
        if(!$pedido){
            throw new \Exception("Ocurrio un error al encontrar el pedido N°".$OrderID);
        }
        $movimientos_venta=Movimiento::where([
            ['id_pedido',"=",$pedido->id],
            ['id_apertura',"=",$caja->id_apertura],
            ['id_motivo_movimiento','=',MotivoMovimiento::$VENTA]
        ]);
        foreach($detalle_pedido as $detalle) StockProductos::updateStock($detalle->id_producto,$detalle->cantidad);
       if($movimientos_venta->count()>0){
           foreach ($movimientos_venta->get() as $movimiento) $descuento_a_caja+= $movimiento->monto;
           $caja->cantidad-=$descuento_a_caja;

          Movimiento::create([
              'id_caja'=>$caja->id,
              'id_pedido'=>$pedido->id,
              'id_apertura'=>$caja->id_apertura,
              'id_usuario'=>Auth::id(),
              'id_motivo_movimiento'=>MotivoMovimiento::$CANCELACION,
              'monto'=>-$descuento_a_caja
          ]);
          $movimientos_vuelto=Movimiento::where([
              ['id_pedido',"=",$pedido->id],
              ['id_apertura',"=",$caja->id_apertura],
              ['id_motivo_movimiento','=',MotivoMovimiento::$VUELTO]
          ])->get();
          if($movimientos_vuelto->count()>0){
              $vuelto_devuelto=0;
              foreach ($movimientos_vuelto as $vueltos) $vuelto_devuelto+=$vueltos->monto;
              if($vuelto_devuelto<0){
                  $cantidad_devuelta=($vuelto_devuelto*-1);
                  $caja->cantidad+=$cantidad_devuelta;
                  Movimiento::create([
                      'id_usuario'=>Auth::id(),
                      'id_caja'=>User::workspace('caja')["id"],
                      'id_pedido'=>$pedido->id,
                      'id_apertura'=>$caja->id_apertura,
                      'id_motivo_movimiento'=>MotivoMovimiento::$CANCELACION,
                      'monto'=>$cantidad_devuelta
                  ]);
              }
          }
           $caja->save();
          if($deleteOrder)$pedido->delete();

       }else{
           if($deleteOrder)$pedido->delete();
       }
        DB::commit();
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function estadoPedido()
    {
        return $this->belongsTo('App\Models\EstadoPedido', 'id_estado_pedido');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sucursal()
    {
        return $this->belongsTo('App\Models\Sucursal', 'id_sucursal');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function detallePedidos()
    {
        return $this->hasMany('App\Models\DetallePedido', 'id_pedido');
    }

}
