<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property integer $estado
 * @property string $descripcion
 * @property string $created_at
 * @property string $updated_at
 * @property Pedido[] $pedidos
 */
class EstadoPedido extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $casts=["estado"=>'boolean'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    public static $PREPARACION=1;
    public static $DESPACHADO=2;
    public static $ENTREGADO=3;
    protected $table = 'estado_pedido';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['estado', 'descripcion', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pedidos()
    {
        return $this->hasMany('App\Models\Pedido', 'id_estado_pedido');
    }
}
