<?php

namespace App\Http\Requests;

use App\Models\Caja;
use App\Models\User;
use Illuminate\Http\Request;

class UpdateCajaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return User::isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Caja::createRule();
    }
}
