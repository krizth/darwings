<?php

namespace App\Http\Controllers;

use App\Models\Descuento;
use App\Models\User;
use Illuminate\Http\Request;

class DescuentosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return array
     */
    public function index()
    {

        return ['rows'=>Descuento::all(),
            "columns"=>[
                ["name"=> "porcentaje",  "label"=> 'Porcentaje',"align"=>"left", "field"=> 'porcentaje', "sortable"=> false],
                ["name"=> 'descripcion',  "label"=> 'Descripcion',"align"=>"left", "field"=> 'descripcion', "sortable"=> true] ,
            ]
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return array[]
     */
    public function create()
    {
        if(!User::isAdmin()) abort(401,"No autorizado");
        return [
            ["name"=> 'porcentaje', "label"=> 'Porcentaje', "field"=> 'porcentaje',"type"=>"number","required"=>true,"value"=>0],
            ["name"=> 'descripcion', "label"=> 'Descripcion', "field"=> 'descripcion',"type"=>"textarea","required"=>true,"value"=>''],
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!User::isAdmin()) abort(401,"No autorizado");
        Descuento::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return array[]
     */
    public function edit($id)
    {
        if(!User::isAdmin()) abort(401,"No autorizado");
        $descuento=Descuento::find($id);
        return [
            ["name"=> 'porcentaje', "label"=> 'Porcentaje', "field"=> 'porcentaje',"type"=>"number","required"=>true,"value"=>$descuento->porcentaje],
            ["name"=> 'descripcion', "label"=> 'Descripcion', "field"=> 'descripcion',"type"=>"textarea","required"=>true,"value"=>$descuento->descripcion],
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Descuento::find($id)->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       Descuento::find($id)->delete();
        return response('',201);
    }
}
