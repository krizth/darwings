<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Collection;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    /**
     * @param Request $request
     * @param Collection $columns
     * @param $model
     * @param $resource
     * @return array
     */
    protected function getSearchedDataWithResource(Request $request, Collection $columns, $model, $resource):array {
        $out=$this->getSearchedData($request,$columns,$model);
        $out['rows'] = $resource::collection($out['rows']);
        return $out;
        $this->validateWith();
    }

    /**
     * @param Request $request
     * @param Collection $columns
     * @param $model
     * @return array
     */
    protected function getSearchedData(Request $request, Collection $columns, $model):array {
        $output=["rows"=>null,"columns"=>$columns];
        if($request['filter']&&$request['field']){
            $field=$columns->filter(function ($el) use($request){
                return $el["name"]===$request['field'];
            })->first();
            $output["rows"] =$model::where($field["name"],'like','%'.$request['filter'].'%')->get();
            return $output;
        }
           if( method_exists($model,'all')){
               $output["rows"] = $model::all();
           }
           if(method_exists($model,'get')){
               $output["rows"] = $model->get();
           }



        return $output;
    }
}
