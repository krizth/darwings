<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthCodes\CreateAuthCodesRequest;
use App\Http\Requests\AuthCodes\IndexAuthCodesRequest;
use App\Http\Requests\AuthCodes\ShowAuthCodesRequest;
use App\Http\Requests\AuthCodes\StoreAuthCodesRequest;
use App\Http\Resources\AuthCodesResource;
use App\Http\Resources\MainResponse;
use App\Models\AuthCodes;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Response as iHttp;
class AuthCodesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param IndexAuthCodesRequest $request
     * @return AuthCodes[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Http\Response
     */
    public function index(IndexAuthCodesRequest $request)
    {
        AuthCodes::whereDate('created_at','<=',Carbon::yesterday())->delete();
        $columns=[
            ["name"=> 'codigo',  "label"=> 'Codigo',"align"=>"center", "field"=> 'codigo', "sortable"=> false] ,
            ["name"=> "creador",  "label"=> 'Creador',"align"=>"center", "field"=> 'creador', "sortable"=> false],
            ["name"=> 'usuario_destino',  "label"=> 'Usuario Destino',"align"=>"center", "field"=> 'usuario_destino', "sortable"=> true] ,
            ["name"=> 'cantidad',  "label"=> 'Cantidad',"align"=>"center", "field"=> 'cantidad', "sortable"=> false] ,
            ["name"=> 'movimiento',  "label"=> 'Movimiento Realizado',"align"=>"center", "field"=> 'movimiento', "sortable"=> false] ,
            ["name"=> 'user_uso',  "label"=> 'Usuario Beneficiado',"align"=>"center", "field"=> 'user_uso', "sortable"=> false] ,
            ["name"=> 'imagen',  "label"=> 'Documento Adjunto',"align"=>"center", "field"=> 'imagen' , "type"=>'image', "sortable"=> false] ,

        ];
        return MainResponse::normalize(
            ['rows'=>AuthCodesResource::collection(AuthCodes::orderBy('created_at','desc')->get()),
                'columns'=>$columns]
        );

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return array
     */
    public function create(CreateAuthCodesRequest $request)
    {
        AuthCodes::whereDate('created_at','<=',Carbon::yesterday())->delete();
        $code=rand(10000,99999);//todo: de cuantos digitos el codigo?
        if(!AuthCodes::where('codigo',$code)->first()){
            return MainResponse::normalize(AuthCodes::create([
                'id_owner'=>Auth::id(),
                'codigo'=>$code,
            ]));
        }else{
            response()->json(
                MainResponse::normalize(
                    [],
                    ["text"=>"Error al Generar un Código Válido, Intentelo nuevamente",
                        "code"=>iHttp::HTTP_INTERNAL_SERVER_ERROR]
                ),
                 iHttp::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAuthCodesRequest $request)
    {

    }

    public function upload(Request $request)
    {
        $path = collect();
        foreach ($request->file() as $file) {
            $path->put($file->getClientOriginalName(), $file->store("images"));
        }
        return MainResponse::normalize(["imagen" => $path->first()]);
    }

    public function deleteImage(Request $request){
        if(AuthCodes::withTrashed()->where('imagen',$request->imagen)->first()||!User::isAdmin()) abort(401,"No Autorizado");
        Storage::delete($request->imagen);
        return response()->json(MainResponse::normalize([],["text"=>"Completado","code"=>iHttp::HTTP_OK]));

    }

    /**
     * Display the specified resource.
     *
     * @param ShowAuthCodesRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse | array
     */
    public function show(ShowAuthCodesRequest $request,$id)
    {
        AuthCodes::whereDate('created_at','<=',Carbon::yesterday())->delete();
        $code=rand(10000,99999);//todo: de cuantos digitos el codigo?
        if(!AuthCodes::where('codigo',$code)->first()){
          return MainResponse::normalize(AuthCodes::create([
                'id_owner'=>Auth::id(),
                'codigo'=>$code,
                'id_user_destino'=>$id
            ]));
        }else{
            return response()->json(
                MainResponse::normalize(
                    [],
                    ["text"=>"Error al generar un código válido, Intentelo nuevamente",
                        "code"=>iHttp::HTTP_INTERNAL_SERVER_ERROR]
                )
            );

        }
    }

    public function images(Request $request, $filename){
        return MainResponse::normalize(AuthCodes::handleImages($request->wantsJson(),$filename));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param StoreAuthCodesRequest $request
     * @param int $id
     * @return void
     */
    public function edit(StoreAuthCodesRequest $request,$id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreAuthCodesRequest $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(CreateAuthCodesRequest $request,$id)
    {
        AuthCodes::find($id)->delete();
    }
}
