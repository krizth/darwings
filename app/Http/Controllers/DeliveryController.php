<?php

namespace App\Http\Controllers;

use App\Models\Delivery;
use App\Models\User;
use Illuminate\Http\Request;

class DeliveryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return array
     */
    public function index(Request $request)
    {
        return ['rows'=>Delivery::all(),
            "columns"=>[
                ["label"=> 'Precio', "name"=> "comision",   "field"=> 'comision', "sortable"=> false],
                ["label"=> 'Color Zona',"name"=> 'color',  "field"=> 'color',"type" =>"color" ,"sortable"=> true] ,
            ]
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return array[]
     */
    public function create()
    {
        if(!User::isAdmin()) abort(401,"No autorizado");
        return [
            ["name"=> 'comision', "label"=> 'Precio', "field"=> 'comision',"type"=>"number","required"=>true,"value"=>0],
            ["name"=> 'color', "label"=> 'Color', "field"=> 'color',"type"=>"color","required"=>true,"value"=>''],
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!User::isAdmin()) abort(401,"No autorizado");
        Delivery::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return array[]
     */
    public function edit($id)
    {
        $delivery=Delivery::find($id);
        return [
            ["name"=> 'comision', "label"=> 'Precio', "field"=> 'comision',"type"=>"number","required"=>true,"value"=>$delivery->comision],
            ["name"=> 'color', "label"=> 'Color', "field"=> 'color',"type"=>"color","required"=>true,"value"=>$delivery->color],
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!User::isAdmin()) abort(401,"No autorizado");
        Delivery::find($id)->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Delivery::find($id)->delete();
    }
}
