<?php

namespace App\Http\Controllers;

use App\Http\Resources\PedidosResource;
use App\Models\Descuento;
use App\Models\Pedido;
use App\Models\Productos;
use App\Models\StockProductos;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return array
     */
    public function index()
    {
        return ["rows"=>Productos::all(),
            "columns"=>[
                ["name"=> 'nombre',  "label"=> 'Nombre',"align"=>"center", "field"=> 'nombre', "sortable"=> true] ,
                        ["name"=> 'precio',  "label"=> 'Precio',"align"=>"center", "field"=> 'precio', "sortable"=> true],
                        ["name"=> 'descripcion',  "label"=> 'Descripcion', "field"=> 'descripcion', "sortable"=> false],
                        ["name"=> 'puntos',  "label"=> "Puntos Darwing's", "field"=> 'puntos', "sortable"=> false]
            ]
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return array
     */
    public function create()
    {
       return [
           ["name"=> 'nombre', "label"=> 'Nombre',"field"=> 'nombre',"type"=>"text","required"=>"true","value"=>''],
           ["name"=> 'precio', "label"=> 'Precio',"field"=> 'precio',"type"=>"number","required"=>"true","value"=>''],
           ["name"=> 'descripcion', "label"=> 'Descripcion', "field"=> 'descripcion',"type"=>"textarea","required"=>"true","value"=>''],
           ["name"=> 'descuento', "label"=> 'Descuento', "field"=> 'descuento',"type"=>"select",
               "required"=>false,"multiple"=>false, "value"=>'',"options"=>Descuento::all(),'optionLabel'=>'descripcion'],
           ["name"=> 'puntos', "label"=> 'Puntos',"field"=> 'puntos',"type"=>"number","required"=>"true","value"=>'0'],
       ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try{
            Productos::create([
                'nombre'=>$request->nombre,
                'descripcion'=>$request->descripcion,
                'precio'=>$request->precio,
                'id_descuento'=>$request->descuento["id"] ?? null,
                'puntos'=>$request->puntos
            ]);
            return response('ok',200);
        }catch (\Exception $e){
            return response('error',500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return array
     */
    public function edit($id)
    {
        $product=Productos::with('descuento')->find($id);
        return [
            ["name"=> 'nombre', "label"=> 'Nombre',"field"=> 'nombre',"type"=>"text","required"=>"true","value"=>$product->nombre],
            ["name"=> 'precio', "label"=> 'Precio',"field"=> 'precio',"type"=>"number","required"=>"true","value"=>$product->precio],
            ["name"=> 'descripcion', "label"=> 'Descripcion',"field"=> 'descripcion',"type"=>"textarea","required"=>false,"value"=>$product->descripcion],
            ["name"=> 'descuento', "label"=> 'Descuento', "field"=> 'descuento',"type"=>"select",
                "required"=>false,"multiple"=>false, "value"=>Descuento::find($product->id_descuento),"options"=>Descuento::all(),'optionLabel'=>'descripcion'],
            ["name"=> 'puntos', "label"=> 'Puntos',"field"=> 'puntos',"type"=>"number","required"=>"true","value"=>$product->puntos],
        ];
    }
    public function find(Request $request)
    {
        if($request->exists('field')&&$request->exists('value')){
            if($request->field==''||$request->value==''){
                return Productos::take(5)->get();
            }
            return Productos::where($request->field,'like','%'.$request->value.'%')->get();
        }else{
            return Productos::all();
        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
          if($request->descuento===null){
              $descuento=null;
          }else{
              $descuento=$request->descuento["id"];
          }
        Productos::find($id)->update([
            'nombre'=>$request->nombre,
            'descripcion'=>$request->descripcion,
            'precio'=>$request->precio,
            'id_descuento'=>$descuento,
            'puntos'=>$request->puntos
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
       Productos::find($id)->delete();
       StockProductos::where('id_producto',$id)->delete();
       return response('',201);
    }
}
