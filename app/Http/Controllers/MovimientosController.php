<?php

namespace App\Http\Controllers;

use App\Http\Resources\MainResponse;
use App\Http\Resources\MovimientosResource;
use App\Models\AuthCodes;
use App\Models\Caja;
use App\Models\MotivoMovimiento;
use App\Models\Movimiento;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MovimientosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return array
     */
    public function create()
    {
        $caja_sucursal=DB::table('caja')
            ->selectRaw('caja.id as id, concat(caja.descripcion,"-->",sucursal.nombre) as descripcion ')
            ->join('caja_sucursal','caja.id',"=",'caja_sucursal.id_caja')
            ->join('sucursal','sucursal.id','=','caja_sucursal.id_sucursal')
            ->get();
       return [
           ["name"=> 'caja', "label"=> 'Caja', "field"=> 'caja',"type"=>"select",
               "required"=>true,"multiple"=>false, "value"=>'',"options"=>$caja_sucursal,'optionLabel'=>'descripcion'],
           ["name"=> 'movimiento', "label"=> 'Movimiento', "field"=> 'movimiento',"type"=>"select",
               "required"=>true,"multiple"=>false, "value"=>'',"options"=>MotivoMovimiento::select('motivo', 'id', 'operador')->where('req_autorizacion', true)->whereNotIn('id', [MotivoMovimiento::$APERTURA, MotivoMovimiento::$CIERRE])->get(),'optionLabel'=>'motivo'],
           ["name"=> 'monto', "label"=> 'Monto',"field"=> 'monto',"type"=>"number","required"=>true,"value"=>0],
       ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws Exception
     */
    public function store(Request $request)
    {
        if (!$request->auth_code) {
            return AuthCodes::requestAuthCode($request);
        }
        if (AuthCodes::useAuthCode($request->auth_code)) {
            $apertura=Caja::find($request->data["caja"]["id"]);
           $Movimiento= Movimiento::create([
                'id_caja'=>$apertura->id,
                'id_apertura'=>$apertura->id_apertura,
                'id_usuario'=>Auth::id(),
                'id_motivo_movimiento'=>$request->data["movimiento"]["id"],
                'monto'=>$request->auth_code["cantidad"]
            ]);
            Caja::cuadrarCaja($request->data["caja"]["id"]);

            return response($Movimiento,201);
        }
        throw new Exception("Código de autorización no válido");

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return array
     */
    public function show($id)
    {
        return MainResponse::normalize(
            ["rows"=>MovimientosResource::collection(Movimiento::with(['motivoMovimiento','user','medioPago'])->where('id_caja',$id)->orderBy('created_at','desc')->get()),
                "columns"=>[
                    ["name"=> 'motivo_movimiento',  "label"=> 'Motivo del movimiento',"align"=>"center", "field"=> 'motivo_movimiento', "sortable"=> true] ,
                    ["name"=> 'monto',  "label"=> 'Monto del movimiento',"align"=>"center", "field"=> 'monto', "sortable"=> true],
                    ["name"=> 'medio_pago',  "label"=> 'Medio de pago',"align"=>"center", "field"=> 'medio_pago', "sortable"=> true],
                    ["name"=> 'usuario',  "label"=> 'Usuario', "field"=> 'usuario', "sortable"=> false],
                    ["name"=> 'comanda',  "label"=> 'Comanda', "field"=> 'comanda', "sortable"=> false],
                    ["name"=> 'fecha',  "label"=> 'Fecha Movimiento', "field"=> 'fecha', "sortable"=> false],

                ]
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //todo:con codigo de autorizacion
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
