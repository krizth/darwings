<?php

namespace App\Http\Controllers;

use App\Http\Resources\DetallePedidoResource;
use App\Http\Resources\MainResponse;
use App\Http\Resources\PedidosResource;
use App\Models\AuthCodes;
use App\Models\Caja;
use App\Models\Cliente;
use App\Models\DetallePedido;
use App\Models\EstadoPedido;
use App\Models\MedioPago;
use App\Models\Pedido;
use App\Models\StockProductos;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;


class PedidosController extends Controller
{

    public function index(Request $request)
    {


        $columns =new Collection([
            ["name" => "id", "label" => 'N° Comanda', "align" => "center", "field" => 'id', "sortable" => true],
            ["name" => 'cliente.nombre', "label" => 'Cliente', "align" => "center", "field" => 'cliente.nombre', "sortable" => false],
            ["name" => 'telefono', "label" => 'Telefono', "align" => "center", "field" => 'telefono', "sortable" => false],
            ["name" => 'direccion', "label" => 'Direccion de pedido', "align" => "center", "field" => 'direccion', "sortable" => true],
            ["name" => 'estado', "label" => 'Estado del pedido', "align" => "center", "field" => 'estado', "sortable" => true],
            ["name" => 'total', "label" => 'Total($)', "field" => 'total', "sortable" => false],
            ["name" => 'comentario', "label" => 'Comentario del pedido', "field" => 'comentario', "sortable" => false],
            ["name" => 'sucursal', "label" => 'Sucursal', "align" => "center", "field" => 'sucursal', "sortable" => false],
            ["name" => 'created_at', "label" => 'Fecha', "align" => "center", "field" => 'created_at', "sortable" => true],
            ["name" => 'pagado', "label" => 'Pagado', "align" => "center", "field" => 'pagado', "sortable" => true],
            ["name" => 'precio_delivery', "label" => 'Delivery', "align" => "center", "field" => 'precio_delivery', "sortable" => false],
            ["name" => 'Detalle Pedido', "detailed" => true, "label" => 'Detalle pedido', "align" => "center", "field" => 'Detalle Pedido', "sortable" => false,]
        ]);
        return MainResponse::normalize($this->getSearchedDataWithResource($request,$columns,Pedido::with([ 'sucursal', 'cliente', 'detallePedidos' => function ($q) {
            return $q->with(['producto']);
        }, 'estadoPedido', 'delivery'])->where('id_sucursal', User::workspace("sucursal")["id"]),PedidosResource::class));


    }

    public function find(Request $request)
    {

    }

    /**
     *
     * Show the form for creating a new resource.
     *
     * @return array
     */
    public function create()
    {
        return [
            ["name" => 'cliente', "label" => 'Cliente', "field" => 'cliente', "type" => "autocomplete",
                "required" => "true", "url" => '/clients/find', "multiple" => false, "searchField" => 'nombre', 'newValue' => true],
            ["name" => 'telefono', "label" => 'Telefono', "field" => 'telefono', "type" => "text", "required" => "false", "value" => ''],
            ["name" => 'direccion', "label" => 'Direccion del pedido', "field" => 'direccion', "type" => "textarea", "required" => "true", "value" => ''],
            ["name" => 'comentario', "label" => 'Comentario', "field" => 'comentario', "type" => "textarea", "required" => false, "value" => ''],
            ["name" => 'detalle_pedido', "label" => 'Detalle Pedido', "visibility" => false, "field" => 'detalle_pedido', "type" => "form",
                "btnlabel" => "Agregar Producto", "url" => '/orderDetail/', "mode" => 'create', "mapping" => 'mapOrderDetail'],
            ["name" => 'pago', "label" => 'Paga Con', "field" => 'pago', "type" => "number", "required" => true, "value" => 0, "mapping" => "mapExchange"],
            ["name" => 'medio_pago', "label" => 'Medio de pago', "field" => 'medio_pago', "type" => "select",
                "required" => true, "multiple" => false, "value" => '', "options" => MedioPago::all(), 'optionLabel' => 'nombre'],
            ["name" => 'precio_delivery', "label" => 'Precio Delivery', "field" => 'precio_delivery', "type" => "number", "required" => false, "value" => 0],
            ["label" => "Pagado", "field" => 'pagado', "type" => "toggle", "required" => true, "value" => false],
        ];
    }

    /**
     *
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return bool[]|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     *
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $caja = Caja::find(User::workspace("caja")["id"]);
        if (!$caja->estado) throw new \Exception("La caja esta cerrada, debe abrirla para poder realizar la accion");
        if ($request->precio_delivery < 0) throw new \Exception("Precio del delivery no puede ser menor a cero");
        DB::beginTransaction();
        // todo: find($request->cliente["id"])
        $client = $request->cliente["id"] !== 0 ? Cliente::where('rut', $request->cliente["rut"])->first() : Cliente::create(['nombre' => $request->cliente["nombre"]]);
      /*  if($client->trashed()){
            $client->restore();
            $client=$client->save();
        }*/
        if (!$request->cliente) throw new \Exception("No se creo un cliente correctamente");
        $sucursal = User::workspace('sucursal')["id"];
        // try{
        $Encabezado = Pedido::create([
            'id_cliente' => $client->id,
            'direccion' => $request->direccion,
            'comentario' => is_null($request->comentario)?"":$request->comentario,
            'telefono' => $request->telefono,
            'id_estado_pedido' => EstadoPedido::$PREPARACION,
            'id_sucursal' => $sucursal,
            'precio_delivery' => $request->precio_delivery,
            'pagado' => $request->pagado,
            'medio_pago' => $request->medio_pago,
            'subtotal' => 0,
            'descuento' => 0,
            'total' => 0,
            'pago'=>$request->pago
        ]);


         $total = Pedido::totalUpdater($request->detalle_pedido, $Encabezado, $request->precio_delivery,false);
        if ($request->pagado) {
            if ($request->pago < $total) {
                if (!$request->auth_code) {
                    return AuthCodes::requestAuthCode($request);
                }
                if (!AuthCodes::useAuthCode($request->auth_code)) {
                    throw new \Exception("Código de autorización no válido");
                } else {

                    Pedido::pagarPedido($Encabezado->id, User::workspace("caja")["id"], $total, true,$request->auth_code["cantidad"]);
                    Cliente::updatePoints($Encabezado,false);
                    DB::commit();
                    return response($Encabezado,201);
                }
            }
            Cliente::updatePoints($Encabezado,false);
            Pedido::pagarPedido($Encabezado->id, User::workspace("caja")["id"], $total);
        }else{
            $Encabezado->total=$total;
            $Encabezado->save();
        }

        DB::commit();
        return response($Encabezado,201);
        /* }catch (\Exception $e){
             DB::rollBack();
             abort(500,$e->getMessage());
         }*/
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return array
     */
    public function show($id)
    {
        return ["rows" => DetallePedidoResource::collection(DetallePedido::with(['productos'])->where('id_pedido', $id)->get()),
            "columns" => [
                ["name" => 'producto', "label" => 'Producto', "align" => "center", "field" => 'producto', "sortable" => true],
                ["name" => 'cantidad', "label" => 'Cantidad pedida', "align" => "center", "field" => 'cantidad', "sortable" => true],
                ["name" => 'descripcion', "label" => 'Descripcion del producto', "field" => 'descripcion', "sortable" => false],
                ["name" => 'fecha', "label" => 'Fecha', "field" => 'fecha', "sortable" => false],
            ]
        ];


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return array[]
     */
    public function edit($id)
    {

        $pedido = Pedido::with('cliente')->find($id);
        if ($pedido->id_estado_pedido == EstadoPedido::$ENTREGADO) {
            abort(500, "No se puede editar un pedido entregado");
        }
        return [
            ["name" => 'cliente', "label" => 'Cliente', "field" => 'nombre', "type" => "autocomplete",
                "required" => "true", "url" => '/clients/find', "multiple" => false, "searchField" => 'nombre', 'selectedItem' => $pedido->cliente],
            ["name" => 'direccion', "label" => 'Direccion del pedido', "field" => 'direccion', "type" => "textarea", "required" => "true", "value" => $pedido->direccion],
            ["name" => 'comentario', "label" => 'Comentario', "field" => 'comentario', "type" => "textarea", "required" => false, "value" => $pedido->comentario],
            ["name" => 'estado_pedido', "label" => 'Estado Pedido', "field" => 'estado_pedido', "type" => "select",
                "required" => true, "multiple" => false, "value" => EstadoPedido::find($pedido->id_estado_pedido), "options" => EstadoPedido::all(), 'optionLabel' => 'descripcion'],
            ["name" => 'pago', "label" => 'Paga Con', "field" => 'pago', "type" => "number", "required" => true, "value" => $pedido->total, "mapping" => "mapExchange"],
            ["label" => "Pagado", "field" => 'pagado', "type" => "toggle", "required" => true, "value" => $pedido->pagado],
            ["name" => 'comanda', "label" => 'Imprimir Comanda', "field" => 'comanda', "type" => "button",
                "url" => '/orderDetail/', "mode" => 'create', "actionName" => 'printOrder', "actionParams" => ["id_pedido" => $id]],
        ];
    }

    public function imprimirPedido($id)
    {
        return Pedido::with(['detallePedidos' => function ($query) {
            return $query->with(['productos']);
        }, 'cliente', 'sucursal'])->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return array|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $caja = Caja::find(User::workspace("caja")["id"]);
        if (!$caja->estado)throw new \Exception("La caja esta cerrada, debe abrirla para poder realizar la accion");
        if(!$id)throw new \Exception("Se requiere especificar un pedido");
        $update_request = ['telefono'=>$request->telefono,'precio_delivery'=>$request->precio_delivery,'medio_pago'=>$request->medio_pago,'comentario' => is_null($request->comentario)?"":$request->comentario, 'direccion' => $request->direccion, 'id_estado_pedido' => $request->estado_pedido["id"], 'pagado' => $request->pagado,'pago'=>$request->pago];
        $pedido_model = Pedido::find($id);
        $existentes=collect();
        if($pedido_model->pagado==true&&$request->pagado==false){
            Cliente::updatePoints($pedido_model,true);
        }
        foreach ($request->detalle_pedido as $producto_pedido) {
            $producto_id=Arr::exists($producto_pedido,'id')?$producto_pedido["id_producto"]:$producto_pedido['producto']['id'];
            $find_product=[['id_pedido','=',$id],['id_producto','=',$producto_id]];
            $detalle_bd=DetallePedido::where($find_product)->first();
            $formated_ped=Arr::except($producto_pedido,["producto","created_at","updated_at"]);

            if($detalle_bd){
                $existentes->push($detalle_bd->id_producto);
                DetallePedido::where($find_product)->update($formated_ped);
                $dif=$detalle_bd->cantidad - $producto_pedido["cantidad"];

            }else{
                $formated_ped["id_pedido"]=$id;
                $formated_ped["id_producto"]=$producto_id;
                $det=DetallePedido::create($formated_ped);
                $existentes->push($det->id_producto);
                $dif=-$det->cantidad;
            }
            StockProductos::updateStock($producto_id,$dif);
        }

        if($existentes->count()<(DetallePedido::where('id_pedido',$id)->get())->count()) {

          $devuelta_al_stock=  DetallePedido::whereNotIn('id_producto',$existentes)->get();
              foreach ($devuelta_al_stock as $item) {
                  StockProductos::updateStock($item->id_producto,$item->cantidad);
              }
              DetallePedido::whereNotIn('id_producto',$existentes)->forceDelete();
        }

        if($pedido_model->pagado==true&&$request->pagado==false){
            if(($pedido_model->id_estado_pedido!==EstadoPedido::$PREPARACION)){
                if(is_null($request->auth_code)){
                    return AuthCodes::requestAuthCode($request);
                }else{
                    AuthCodes::useAuthCode($request->auth_code,$request->pago);
                }
            }
            Pedido::cancelarPedido($id,false);
        }else{
            if(Pedido::find($id)->update($update_request)){
                $pedido=Pedido::find($id);
                Pedido::totalUpdater(null,$pedido,$pedido->precio_delivery,false);
                Cliente::updatePoints($pedido_model,false);
                Pedido::pagarPedido($pedido->id, User::workspace("caja")["id"], $pedido->pago);
            }else{
                return abort(500);
            }
        }
        return response(Pedido::find($id),200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return array|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $caja = Caja::find(User::workspace("caja")["id"]);
        if (!$caja->estado) {
            DB::rollBack();
            abort(500, "La caja esta cerrada, debe abrirla para poder realizar la acción");
        }
        try {
            DB::beginTransaction();
            $pedido = Pedido::find($id);
            if($pedido->pagado&&$pedido->id_estado_pedido!==EstadoPedido::$PREPARACION){
                if(is_null($request->auth_code))return AuthCodes::requestAuthCode($request);
                if (!AuthCodes::useAuthCode($request->auth_code)) {
                    throw new \Exception("Código de autorización no válido");
                }
            }
            Pedido::cancelarPedido($pedido->id);
            Cliente::updatePoints($pedido,true);
            DB::commit();
            return response('ok',201);

        } catch (\Exception $e) {
            DB::rollBack();
            abort(500, $e->getMessage());
        }
    }
}
