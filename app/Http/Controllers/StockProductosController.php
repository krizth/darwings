<?php

namespace App\Http\Controllers;

use App\Http\Resources\StockProductosFindResource;
use App\Http\Resources\StockProductosResource;
use App\Models\StockProductos;
use App\Models\User;
use Illuminate\Http\Request;

class StockProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return array
     */
    public function index()
    {
        return ["rows"=>StockProductosResource::collection(StockProductos::with('producto')->where('id_sucursal',User::workspace('sucursal')["id"])->get()),
            "columns"=>[
            ["label"=>"Cantidad","field"=>'cantidad', "name"=> 'cantidad', "sortable"=> true],
            ["label"=>"Nombre Producto","field"=>'nombre',"name"=> 'nombre', "sortable"=> true],
            ["label"=>"Descripcion","field"=>'descripcion',"name"=> 'descripcion', "sortable"=> true],
        ]];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return array
     */
    public function create(Request $request)
    {
        return [
            ["name"=> 'producto', "label"=> 'Producto', "field"=>'producto',"type"=>"autocomplete",
                "required"=>true,"url"=>'/products/find',"multiple"=>false, "searchField"=>'nombre'],
            ["label"=>"Cantidad","field"=>'cantidad',"type"=>"number","required"=>true,"value"=>"0"],
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $stock=StockProductos::withTrashed()->where([
            ["id_producto",$request->producto["id"]],
            ["id_sucursal",User::workspace("sucursal")["id"]]
        ])->first();

        if($stock){
            $stock->update([
                "cantidad"=> $request->cantidad,
                "id_sucursal"=>User::workspace("sucursal")["id"],
            ]);
            if($stock->trashed()){
                $stock->restore();
                $stock->save();
            }
        }else{
            StockProductos::create([
                "id_producto"=>$request->producto["id"],
                "cantidad"=>$request->cantidad,
                "id_sucursal"=>User::workspace("sucursal")["id"],
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return array
     */
    public function edit($id)
    {
        $stock_productos=StockProductos::with(["producto"])->find($id);
        return [
            ["name"=> 'producto', "label"=> 'Producto', "field"=>'producto',"type"=>"autocomplete",
                "required"=>"true","url"=>'/products/find',"multiple"=>false, 'selectedItem'=>$stock_productos->producto, "searchField"=>'nombre'],
            ["label"=>"Cantidad","field"=>'cantidad',"type"=>"number","required"=>"true","value"=>$stock_productos->cantidad],
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        StockProductos::find($id)->update([
            'cantidad'=>$request->cantidad
        ]);
    }
    public function find(Request $request)
    {
        if($request->exists('field')&&$request->exists('value')){
            if($request->field==''||$request->value==''){
                return StockProductosFindResource::collection(StockProductos::with(['producto'])->where([
                    ['cantidad',">",0],
                    ['id_sucursal',"=",User::workspace("sucursal")["id"]]
                ])->get());
            }
            return StockProductosFindResource::collection(StockProductos::with(['producto'=>function($query)use($request){
                return $query->where($request->field,'like','%'.$request->value.'%');
            }])->where([
                ['cantidad',">",0],
                ['id_sucursal',"=",User::workspace("sucursal")["id"]]
            ])->get());
        }else{
            return StockProductosFindResource::collection(StockProductos::with('producto')->where([
                ['cantidad',">",0],
                ['id_sucursal',"=",User::workspace("sucursal")["id"]]
            ])->get());
        }

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        StockProductos::find($id)->delete();
    }
}
