<?php

namespace App\Http\Controllers;

use App\Http\Requests\Caja\EditCajaRequest;
use App\Http\Requests\SearchRequest;
use App\Http\Requests\UpdateCajaRequest;
use App\Http\Resources\CajaResource;
use App\Http\Resources\MainResponse;
use App\Models\Caja;
use App\Models\CajaSucursal;
use App\Models\CajaUsuario;
use App\Models\MotivoMovimiento;
use App\Models\Movimiento;
use App\Models\Productos;
use App\Models\Sucursal;
use App\Models\User;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Illuminate\Http\Response as iHttp;

class CajaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param SearchRequest $request
     * @return array
     */


    public function index(SearchRequest $request)
    {
        if(!User::isAdmin()){
            return app('App\Http\Controllers\MovimientosController')->show(User::workspace("caja")["id"]);
        }
       $columns= new Collection([
           ["name"=> "sucursal",  "label"=> 'Nombre de sucursal',"align"=>"center", "field"=> 'sucursal', "sortable"=> false],
           ["name"=> 'descripcion',  "label"=> 'Descripcion caja',"align"=>"center", "field"=> 'descripcion', "sortable"=> true] ,
           ["name"=> 'cantidad',  "label"=> 'Cantidad en caja',"align"=>"center", "field"=> 'cantidad', "sortable"=> false] ,
           ["name"=> 'estado',  "label"=> 'Abierta',"align"=>"center", "field"=> 'estado', "sortable"=> true],
           ["name"=> 'Movimientos en caja',  "label"=> 'Detalle movimientos',"align"=>"center", "field"=> 'detalle', "sortable"=> true]
       ]);
        return MainResponse::normalize($this->getSearchedDataWithResource($request,$columns,Caja::class,CajaResource::class));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return array[]
     */
    public function create()
    {
        return MainResponse::normalize([
            ["name"=> 'sucursal', "label"=> 'Sucursal', "field"=> 'sucursal',"type"=>"select",
                "required"=>true,"multiple"=>false, "value"=>'',"options"=>Sucursal::all(),'optionLabel'=>'nombre'],
            ["name"=> 'descripcion', "label"=> 'Descripcion', "field"=> 'descripcion',"type"=>"textarea","required"=>true,"value"=>''],
            ["name"=> 'cantidad', "label"=> 'Cantidad de inicio',"field"=> 'cantidad',"type"=>"number","required"=>true,"value"=>'0'],
            ["name"=> 'estado',"label"=>"Estado inicial","field"=>'estado',"type"=>"toggle","required"=>true,"value"=>false],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validation=Validator::make($request->all(),Caja::createRule());
        if(!$validation->fails()){
            $caja=Caja::create(["descripcion"=>$request->descripcion,
                "cantidad"=>$request->cantidad,
                "estado"=>$request->estado,
                'id_sucursal'=>$request->sucursal["id"]
            ]);
            if($request->estado){
                $apertura=Movimiento::create([
                    "id_caja"=>$caja->id,
                    "id_apertura"=>$caja->id_apertura,
                    "id_motivo_movimiento"=>MotivoMovimiento::$APERTURA,
                    "monto"=>0,
                    "id_usuario"=>Auth::id()
                ]);
                $apertura->id_apertura=$apertura->id;
                $caja->id_apertura=$apertura->id;
                $caja->cantidad=$request->cantidad;
                $apertura->save();
            }
            if($request->cantidad >0){
                Movimiento::create([
                    "id_caja"=>$caja->id,
                    "id_apertura"=>$caja->id_apertura,
                    "id_motivo_movimiento"=>MotivoMovimiento::$AJUSTE,
                    "id_usuario"=>Auth::id(),
                    "monto"=>$request->cantidad
                ]);
            }
        }else{

            return response()
                ->json(MainResponse::normalize(
                    ['message' => implode(',',$validation->messages()->all())],
                    ['text'=>"Error","code"=>iHttp::HTTP_NOT_FOUND]),
                    iHttp::HTTP_NOT_FOUND);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param EditCajaRequest $request authorize if user is admin
     * @param int $id
     * @return array[]
     */
    public function edit(EditCajaRequest $request,$id)
    {
        $caja=Caja::with('sucursal')->find($id);
        return MainResponse::normalize([
            ["name"=> 'sucursal', "label"=> 'Sucursal', "field"=> 'sucursal',"type"=>"select",
                "required"=>true,"multiple"=>false, "value"=>$caja->sucursal,"options"=>Sucursal::all(),'optionLabel'=>'nombre'],
            ["name"=> 'descripcion', "label"=> 'Descripcion', "field"=> 'descripcion',"type"=>"textarea","required"=>true,"value"=>$caja->descripcion],
            ["name"=> 'cantidad', "label"=> 'Cantidad Actual',"field"=> 'cantidad',"type"=>"number","required"=>true,"value"=>$caja->cantidad],
            ["label"=>"Estado","field"=>'estado',"type"=>"toggle","required"=>true,"value"=>$caja->estado],
        ]);
    }

    public function update(UpdateCajaRequest $request, $id)
    {
        if(!User::isAdmin()) abort(401,"No autorizado");
        try{
            DB::beginTransaction();
            $caja=Caja::find($id);
            $caja->descripcion=$request->descripcion;
            if(($caja->cantidad!==$request->cantidad)){
                Movimiento::create([
                    "id_caja"=>$caja->id,
                    "id_apertura"=>$caja->id_apertura,
                    "id_motivo_movimiento"=>MotivoMovimiento::$AJUSTE,
                    "id_usuario"=>Auth::id(),
                    "monto"=>($request->cantidad - $caja->cantidad)
                ]);
                $caja->cantidad=$request->cantidad;
            }
            if($caja->estado&&(!$request->estado)){
                $total=Movimiento::where([
                    ["id_apertura","=",$caja->id_apertura],
                    ["id_caja","=",$caja->id],
                ])->whereIn("id_motivo_movimiento",
                    [   MotivoMovimiento::$VENTA,
                        MotivoMovimiento::$CANCELACION,
                        MotivoMovimiento::$AJUSTE,
                        MotivoMovimiento::$ENTRADA,
                        MotivoMovimiento::$APERTURA,
                        MotivoMovimiento::$DELIVERY]
                )->get()->sum("monto");
               $cierre=Movimiento::create([
                "id_caja"=>$caja->id,
                "id_apertura"=>$caja->id_apertura,
                "id_usuario"=>Auth::id(),
                "id_motivo_movimiento"=>MotivoMovimiento::$CIERRE,
                   "monto"=>$total
               ]);
                Movimiento::where([
                    ["id_apertura","=",$caja->id_apertura],
                    ["id_caja","=",$caja->id],
                ])->update([
                    'id_cierre'=>$cierre->id
                ]);
                $caja->id_ultimo_cierre=$cierre->id;
                $caja->id_apertura=null;
//-------------------------------------------reset descuentos-----------------------------------------------------------
                Productos::whereNotNull('id_descuento')->update([
                    'id_descuento'=>null
                ]);
//----------------------------------------------------------------------------------------------------------------------
            }
            if((!$caja->estado)&&$request->estado){
                $apertura=Movimiento::create([
                    "id_caja"=>$caja->id,
                    "id_apertura"=>$caja->id_apertura,
                    "id_motivo_movimiento"=>MotivoMovimiento::$APERTURA,
                    "monto"=>$request->cantidad,
                    "id_usuario"=>Auth::id()
                ]);
                $apertura->id_apertura=$apertura->id;
                $caja->id_apertura=$apertura->id;
                $caja->cantidad=$request->cantidad;
                $apertura->save();
            }
            $caja->estado=$request->estado;
            $caja->save();
            DB::commit();

        }catch (\Exception $e){
            DB::rollBack();
            abort(500,"Error al actualizar la caja");
        }
    }

    public function currency(){
     return (Caja::find(User::workspace("caja")["id"]));
    }

    public function informe(){
       $informe= DB::select('SELECT
                users.`name` nombre_usuario,
                medio_pago.nombre medio_pago,
                motivo_movimiento.motivo motivo_movimiento,
                pedido.total total_pedido,
                cliente.nombre nombre_cliente,
                case cliente.amigos_de_darwings when 0 then "No" when 1 then "Si" else "" end as amigos_de_darwings,
                movimiento.monto monto_movimiento,
                sucursal.nombre sucursal,
                movimiento.id_pedido pedido,
                pedido.subtotal,
                pedido.descuento,
                pedido.precio_delivery
                FROM
                movimiento
                INNER JOIN caja ON movimiento.id_caja = caja.id
                INNER JOIN caja_sucursal ON caja_sucursal.id_caja = caja.id
                INNER JOIN sucursal ON caja_sucursal.id_sucursal = sucursal.id
                INNER JOIN users ON movimiento.id_usuario = users.id
                INNER JOIN motivo_movimiento ON movimiento.id_motivo_movimiento = motivo_movimiento.id AND movimiento.id_motivo_movimiento <> '.MotivoMovimiento::$APERTURA.'
                LEFT JOIN pedido ON movimiento.id_pedido = pedido.id
                LEFT JOIN medio_pago ON movimiento.id_medio_pago = medio_pago.id
                LEFT JOIN cliente ON pedido.id_cliente = cliente.id');

        return ['rows'=>$informe,
            "columns"=>[
                ["name"=> "nombre_usuario",  "label"=> 'Nombre de usuario',"align"=>"center", "field"=> 'nombre_usuario'],
                ["name"=> 'medio_pago',  "label"=> 'Medio de pago',"align"=>"center", "field"=> 'medio_pago', "sortable"=> true] ,
                ["name"=> 'motivo_movimiento',  "label"=> 'Motivo movimiento',"align"=>"center", "field"=> 'motivo_movimiento', "sortable"=> false] ,
                ["name"=> 'total_pedido',  "label"=> 'Total Pedido',"align"=>"center", "field"=> 'total_pedido', "sortable"=> true],
                ["name"=> 'nombre_cliente',  "label"=> 'Nombre Cliente',"align"=>"center", "field"=> 'nombre_cliente', "sortable"=> true],
                ["name"=> 'amigos_de_darwings',  "label"=> 'Amigos de Darwings',"align"=>"center", "field"=> 'amigos_de_darwings', "sortable"=> true],
                ["name"=> 'monto_movimiento',  "label"=> 'Monto Movimiento',"align"=>"center", "field"=> 'monto_movimiento', "sortable"=> true],
                ["name"=> 'sucursal',  "label"=> 'Sucursal',"align"=>"center", "field"=> 'sucursal', "sortable"=> true],
                ["name"=> 'pedido',  "label"=> 'Comanda',"align"=>"center", "field"=> 'pedido', "sortable"=> true],
                ["name"=> 'precio_delivery',  "label"=> 'Delivery',"align"=>"center", "field"=> 'precio_delivery', "sortable"=> true],
                ["name"=> 'subtotal',  "label"=> 'Subtotal',"align"=>"center", "field"=> 'subtotal', "sortable"=> true],
                ["name"=> 'descuento',  "label"=> 'Descuento',"align"=>"center", "field"=> 'descuento', "sortable"=> true],

            ]
        ];
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!User::isAdmin()) abort(401,"No autorizado");
       $caja= Caja::find($id);
       CajaSucursal::where('id_caja',$caja->id)->delete();
       CajaUsuario::where('id_caja',$caja->id)->delete();
       $caja->delete();

    }
}
