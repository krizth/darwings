<?php

namespace App\Http\Controllers;

use App\Http\Resources\MainResponse;
use Illuminate\Http\Request;
use App\Models\Cliente;
use Illuminate\Support\Collection;

class ClientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return array
     */
    public function index(Request $request)
    {
        $columns=new Collection([
            ["label"=>"Nombre","field"=>'nombre',"name"=> 'nombre', "sortable"=> true] ,
            ["label"=>"Rut","field"=>'rut', "name"=> 'rut', "sortable"=> true],
            ["label"=>"Direccion","field"=>'direccion', "name"=> 'direccion', "sortable"=> false],
            ["label"=>"Telefono","field"=>'telefono', "name"=> 'telefono', "sortable"=> false],
            ["label"=>"Amigos de DarWings","field"=>'amigos_de_darwings', "name"=> 'amigos_de_darwings', "sortable"=> false],
            ["label"=>"Puntos","field"=>'puntos',"name"=>"puntos", "sortable"=> false],
            ["label"=>"Ciudad","field"=>'ciudad', "name"=> 'ciudad', "sortable"=> false]
        ]);
        return MainResponse::normalize($this->getSearchedData($request,$columns,Cliente::class));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return array
     */
    public function create()
    {
        return [
            ["label"=>"Nombre","field"=>'nombre',"type"=>"text","required"=>"true","value"=>""],
            ["label"=>"Rut","field"=>'rut',"type"=>"text","required"=>"false","value"=>""],
            ["label"=>"Direccion","field"=>'direccion',"type"=>"text","required"=>"false","value"=>""],
            ["label"=>"Telefono","field"=>'telefono',"type"=>"text","required"=>"false","value"=>""],
            ["label"=>"Amigos de DarWings","field"=>'amigos_de_darwings',"type"=>"toggle","required"=>"false","value"=>0],
            ["label"=>"Puntos","field"=>'puntos',"type"=>"number","required"=>"false","value"=>"","min"=>0],
            ["label"=>"Ciudad","field"=>'ciudad',"type"=>"text","required"=>"false","value"=>""],
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return  Cliente::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Cliente::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return array
     */
    public function edit($id)
    {
        $cliente=Cliente::find($id);
        return [
            ["label"=>"Nombre","field"=>'nombre',"type"=>"text","required"=>"true","value"=>$cliente->nombre],
            ["label"=>"Rut","field"=>'rut',"type"=>"text","required"=>"false","value"=>$cliente->rut],
            ["label"=>"Direccion","field"=>'direccion',"type"=>"text","required"=>"false","value"=>$cliente->direccion],
            ["label"=>"Telefono","field"=>'telefono',"type"=>"text","required"=>"false","value"=>$cliente->telefono],
            ["label"=>"Amigos de DarWings","field"=>'amigos_de_darwings',"type"=>"toggle","required"=>"false","value"=>$cliente->amigos_de_darwings],
            ["label"=>"Puntos","field"=>'puntos',"type"=>"number","required"=>"false", "min"=>0 ,"value"=>$cliente->puntos],
            ["label"=>"Ciudad","field"=>'ciudad',"type"=>"text","required"=>"false","value"=>$cliente->ciudad],
            ["label"=>"Ubicacion","field"=>'location',"type"=>"map","required"=>"false","value"=>$cliente->locacion],
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->puntos<0){
            abort(500,"Puntos no puede ser menor a cero");
        }
        return Cliente::where('id',$id)->update($request->all());
    }

    public function find(Request $request)
    {
        if($request->field==''||$request->value==''){
            return Cliente::take(5)->get();
        }
        return Cliente::where($request->field,'like','%'.$request->value.'%')->get();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cliente::find($id)->delete();
        return response('',201);
    }
}
