<?php

namespace App\Http\Controllers;

use App\Http\Resources\DetallePedidoResource;
use App\Models\Cliente;
use App\Models\DetallePedido;
use App\Models\EstadoPedido;
use App\Models\MotivoMovimiento;
use App\Models\Movimiento;
use App\Models\Pedido;
use App\Models\Productos;
use App\Models\StockProductos;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DetallePedidoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return array
     */
    public function create()
    {
        return [
            ["name"=> 'producto', "label"=> 'Producto en Stock', "field"=> 'producto',"type"=>"autocomplete",
                "required"=>true,"url"=>'/stockProduct/find',"multiple"=>false, "searchField"=>'nombre',],
            ["name"=> 'cantidad',"label"=>"Cantidad","field"=>'cantidad',"type"=>"number","required"=>"true","value"=>0],
            ["name"=> 'descripcion',"label"=>"Comentario (Opcional)","field"=>'descripcion',"type"=>"textarea","required"=>false,"value"=>""],
            ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->producto == null) throw new \Exception("Debe seleccionar un producto");
        $det=DetallePedido::where([
            ['id_pedido',"=",$request->id],
            ['id_producto',"=",$request->producto["id"]]
        ])->first();
        DB::beginTransaction();
        if($det){
            $det->cantidad+=$request->cantidad;
            $det->save();
        }else{
            DetallePedido::create([
                'id_pedido'=>$request->id,
                'id_producto'=>$request->producto["id"],
                'cantidad'=>$request->cantidad,
                'descripcion'=>$request->descripcion===null?"":$request->descripcion
            ]);
        }
        try {
            $Encabezado=Pedido::find($request->id);
            Pedido::totalUpdater(null,$Encabezado,$Encabezado->precio_delivery);
            StockProductos::updateStock($request->producto["id"],-$request->cantidad);
            DB::commit();
            return response("",200);
        } catch (\Exception $e) {
            DB::rollBack();
            abort(500,$e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id order id
     * @return array
     */
    public function show($id)
    {
        return ["rows"=>DetallePedidoResource::collection(DetallePedido::with(['productos'])->where('id_pedido',$id)->get()),
            "columns"=>[
                ["name"=> 'producto',  "label"=> 'Producto',"align"=>"center", "field"=> 'producto', "sortable"=> true] ,
                ["name"=> 'cantidad',  "label"=> 'Cantidad pedida',"align"=>"center", "field"=> 'cantidad', "sortable"=> true],
                ["name"=> 'descripcion',  "label"=> 'Descripcion del producto', "field"=> 'descripcion', "sortable"=> false],
                ["name"=> 'comentario',  "label"=> 'Comentario', "field"=> 'comentario', "sortable"=> false],
            ]
            ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return array[]
     */
    public function edit($id)
    {
        $detalle=DetallePedido::with(['productos'])->find($id);
        return [
            ["name"=> 'detalle_pedido', "label"=> 'Producto', "field"=> 'detalle_pedido',"type"=>"autocomplete",
                "required"=>"true","url"=>'/products/find',"multiple"=>false, "searchField"=>'nombre','selectedItem'=>$detalle->productos],
            ["name"=> 'cantidad',"label"=>"Cantidad","field"=>'cantidad',"type"=>"number","required"=>true,"value"=>$detalle->cantidad],
            ["name"=> 'descripcion',"label"=>"Comentario","field"=>'descripcion',"type"=>"textarea","required"=>false,"value"=>$detalle->descripcion],
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->cantidad>0){
            $detalle_pedido=DetallePedido::find($id);
            $dif=$detalle_pedido->cantidad-$request->cantidad;
            if($request->detalle_pedido["id"]!==null){
                $detalle_pedido->update(['id_producto'=>$request->detalle_pedido["id"],"cantidad"=>$request->cantidad,"comentario"=>$request->comentario?$request->comentario:""]);
            }else{
                $detalle_pedido->update(["cantidad"=>$request->cantidad,"comentario"=>$request->comentario?$request->comentario:""]);
            }
           // try {
                $Encabezado=Pedido::find($detalle_pedido->id_pedido);
                Pedido::totalUpdater(null,$Encabezado,$Encabezado->precio_delivery);
                StockProductos::updateStock($request->detalle_pedido["id"],$dif);
                Cliente::updatePoints($Encabezado,$dif<0);
            /*} catch (\Exception $e) {
                abort(500,$e->getMessage());
            }*/
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            $detalle_pedido=DetallePedido::find($id);
            $status=Pedido::find($detalle_pedido->id_pedido);
            if($status->id_estado_pedido===EstadoPedido::$ENTREGADO&&!User::isAdmin()){
                throw new \Exception("No se puede eliminar un detalle de un pedido entregado");
            }
            if($status->pagado&&!User::isAdmin()){
                throw new \Exception("No se puede eliminar un detalle de un pedido pagado");
            }
            StockProductos::updateStock($detalle_pedido->id_producto,$detalle_pedido->cantidad);
            $pedido=Pedido::find($detalle_pedido->id_pedido);
            $detalle_pedido->delete();
            if(DetallePedido::where('id_pedido',$pedido->id)->count()==0){
                $pedido->delete();

            }
            Pedido::totalUpdater(null,$pedido,0);
            return response('',201);

    }
}
