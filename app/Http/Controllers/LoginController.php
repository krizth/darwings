<?php

namespace App\Http\Controllers;

use App\Models\Caja;
use App\Models\CajaSucursal;
use App\Models\CajaUsuario;
use App\Models\SucursalesUsuario;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;


class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return array
     */
    public function postLoginOptions()
    {
        try{
            $cajas=CajaUsuario::with(['caja'=>function($q){
                return $q->with('sucursal');
            }])->where('id_users',Auth::id())->get();
            return $cajas;
        }catch (\Exception $ex){
            return "Error:".$ex->getMessage();
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return array
     */
    public function postLoginParamsVerification(Request $request)
    {
        $cajaSucursal=Caja::where(['id'=>$request["caja"]["id"],'id_sucursal'=>$request["sucursal"]["id"]])->exists();
        if($cajaSucursal)
        {
            $user=User::find(Auth::id());
            $user->workspace=["sucursal"=>$request->sucursal,"caja"=>$request->caja];
            $user->save();
            return ["sucursal"=>$request->sucursal,"caja"=>$request->caja,"usuario"=>User::find(Auth::id())];
        }

        else{abort(500);}

    }
    public function logout(Request $request)
    {
        $user=User::find(Auth::id());
        $user->workspace=null;
        $user->save();
        $request->user()->token()->revoke();
        $json = [
            'success' => true,
            'code' => 200,
            'message' => 'You are Logged out.',
        ];
        return response()->json($json, '200');
    }
    protected function guard()
    {
        return Auth::guard('api');
    }
    public function verify(){
        if(Auth::user()){
            response('',200);
        }else{
            abort(402);
        }
    }
}
