<?php

namespace App\Http\Controllers;

use App\Models\Caja;
use App\Models\CajaSucursal;
use App\Models\CajaUsuario;
use App\Models\Sucursal;
use App\Models\SucursalesUsuario;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return User[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index(Request $request)
    {
        if(!User::isAdmin()) abort(401,"No autorizado");
        $columns= new Collection([
            ["name"=> 'name',  "label"=> 'Nombre',"align"=>"center", "field"=> 'name', "sortable"=> true] ,
            ["name"=> 'email',  "label"=> 'Correo',"align"=>"center", "field"=> 'email', "sortable"=> true],
            ["name"=> 'created_at',  "label"=> 'Creacion', "field"=> 'created_at', "sortable"=> false]

        ]);
        return $this->getSearchedData($request,$columns,User::class);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return array
     */
    public function create()
    {
        if(!User::isAdmin()) abort(401,"No autorizado");
        $cs=CajaSucursal::with(['caja'])->where('id_sucursal',User::workspace("sucursal")["id"])->get();
        $cajas=collect();
        foreach ($cs as $reg) $cajas->push($reg->caja);
        return [
            ["name"=> 'name',"label"=>"Nombre","field"=>'name',"type"=>"text","required"=>"true","value"=>''],
            ["name"=> 'email',"label"=>"Email","field"=>'email',"type"=>"email","required"=>"true","value"=>''],
            ["name"=> 'password',"label"=>"Contraseña","field"=>'password',"type"=>"password","required"=>"true","value"=>''],
            ["name"=> 'sucursal', "label"=> 'Sucursal', "field"=> 'sucursal',"type"=>"select",
                "disable"=>true,"required"=>true,"multiple"=>false, "value"=>User::workspace("sucursal"),"options"=>[],'optionLabel'=>'nombre'],
            ["name"=> 'caja', "label"=> 'Caja', "field"=> 'caja',"type"=>"select",
                "disable"=>!User::isAdmin(),"required"=>true,"multiple"=>true, "value"=>null,"options"=>$cajas,'optionLabel'=>'descripcion'],
           User::isAdmin()? ["name"=> 'isAdmin',"label"=>"Administrador","field"=>'isAdmin',"type"=>"toggle","required"=>false,"value"=>false]:[],
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!User::isAdmin()) abort(401,"No autorizado");
        $data=$request->all();
        $userData=$data;
        unset($userData["sucursal"]);
        unset($userData["caja"]);
        User::withTrashed()->updateOrCreate(['email'=>$userData["email"]],array_merge($userData,
            ['deleted_at'=>null,
                'isAdmin'=>$request->isAdmin,
                "password"=>Hash::make($request->password)]))->restore();
        $user=User::where('email',$userData["email"])->first();
           SucursalesUsuario::withTrashed()->updateOrCreate([
               'id_sucursal'=>$data['sucursal']["id"],
               'id_usuario'=>$user->id
           ],[
               'id_sucursal'=>$data['sucursal']["id"],
               'id_usuario'=>$user->id,
               'deleted_at'=>null
           ])->restore();
           foreach ($request->caja as $caja){
               $cajaUsuario=CajaUsuario::withTrashed()->where([['id_caja',"=",$caja["id"]], ['id_users',"=",$user->id]])->first();
               if(!$cajaUsuario){
                   CajaUsuario::create(['id_caja'=>$caja["id"], 'id_users'=>$user->id]);
               }else{
                   $cajaUsuario->restore();
               }
           }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return User::find($id);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return array[]
     */
    public function edit($id)
    {
        if(!User::isAdmin()){
            $user=User::find(Auth::id());
        }else{
            $user=User::find($id);
        }
        $cajaSucursal=CajaSucursal::with(["caja"])->where([
            ["id_sucursal","=",User::workspace("sucursal")["id"]]
        ])->get();
        $cs=collect();
        foreach($cajaSucursal as $c) $cs->push($c->caja);
        $cajas=DB::select("SELECT
                caja_usuario.id_users,
                caja.descripcion,
                caja.id,
                caja_sucursal.id_sucursal
            FROM
                caja
                INNER JOIN caja_usuario ON caja_usuario.id_caja = caja.id
                INNER JOIN caja_sucursal ON caja_sucursal.id_caja = caja.id
                AND caja_usuario.id_users =:user_id
	            AND caja_sucursal.id_sucursal = :sucursal_id",["user_id"=>$id,"sucursal_id"=>User::workspace("sucursal")["id"]]);
        return [
            ["name"=> 'name',"label"=>"Nombre","field"=>'name',"type"=>"text","required"=>"true","value"=>$user->name],
            ["name"=> 'email',"label"=>"Email","field"=>'email',"type"=>"email","required"=>"true","value"=>$user->email],
            ["name"=> 'sucursal', "label"=> 'Sucursal', "field"=> 'sucursal',"type"=>"select",
                "disable"=>true,"required"=>true,"multiple"=>false, "value"=>User::workspace("sucursal"),"options"=>[],'optionLabel'=>'nombre'],
            ["name"=> 'cajas', "label"=> 'Cajas', "field"=> 'cajas',"type"=>"select",
                "disable"=>!User::isAdmin(),"required"=>true,"multiple"=>true, "value"=>$cajas,"options"=>$cs,'optionLabel'=>'descripcion'],
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user=User::find($id);
        $user->name=$request->name;
        $user->email=$request->email;
        if(User::isAdmin()){
            foreach($request->cajas as $caja){
                CajaUsuario::withTrashed()->UpdateOrcreate(["id_caja"=>$caja["id"],"id_users"=>$id],["id_caja"=>$caja["id"],"id_users"=>$id,'deleted_at'=>null]);
                $cajaSucursal=CajaSucursal::where('id_caja',$caja['id'])->first();
                SucursalesUsuario::firstOrNew(['id_usuario'=>$id,'id_sucursal'=>$cajaSucursal->id_sucursal]);
            }
        }
        $user->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!User::isAdmin()) abort(401,"No autorizado");
        SucursalesUsuario::where('id_usuario',$id)->delete();
        CajaUsuario::where('id_users',$id)->delete();
        User::find($id)->delete();
        return response('',201);
    }
}
