<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StockProductosFindResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->producto!==null?[
            'id'=>$this->producto->id,
            'nombre'=>$this->producto->nombre,
            'precio'=>$this->producto->precio,
            'descripcion'=>$this->producto->descripcion,
        ]:[];
    }
}
