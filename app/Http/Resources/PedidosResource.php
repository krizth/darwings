<?php

namespace App\Http\Resources;

use App\Models\EstadoPedido;
use App\Models\MotivoMovimiento;
use App\Models\Movimiento;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class PedidosResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        if($this->estadoPedido->id===EstadoPedido::$PREPARACION){
            if(Carbon::now()->gt(Carbon::parse($this->created_at)->addHour())){
                $color=["created_at","red"];
            }else{
                $color=null;
            }
        }else{
            if($this->estadoPedido->id===EstadoPedido::$DESPACHADO){
                $color=["estado","green"];
            }else{
                $color=null;
            }
        }

        return [
            'cliente.nombre'=>is_object($this->cliente)?$this->cliente->nombre:'',
            'cliente'=>$this->cliente,
            'direccion'=>$this->direccion,
            'telefono'=>$this->telefono,
                'total'=>$this->total,
                'comentario'=>$this->comentario,
                'sucursal'=>is_object($this->sucursal)?$this->sucursal->nombre:'',
                "created_at"=>Carbon::now()->gt(Carbon::parse($this->created_at)->addHour())&&$this->estadoPedido->id===EstadoPedido::$PREPARACION?Carbon::parse($this->created_at)->format('Y-m-d H:i:s').' (Atrasado)':Carbon::parse($this->created_at)->format('Y-m-d H:i:s'),
                "color"=>$color,
                'id'=>$this->id,
                'pagado'=>$this->pagado,
                'estado'=>is_object($this->estadoPedido)?$this->estadoPedido->descripcion:'',
                'nombre_delivery'=>$this->delivery!==null?$this->delivery->nombre:'',
                'delivery'=>$this->delivery,
                'Detalle Pedido'=>["route"=>'/orderDetail/'],
                'detalle_pedido'=>$this->detallePedidos,
                'medio_pago'=>$this->medio_pago,//todo:corregir medio pago
                'subtotal'=>$this->subtotal,
                'precio_delivery'=>$this->precio_delivery,
                'pago'=>$this->pago,
                'estado_pedido'=>$this->estadoPedido
            ];
    }
}
