<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CajaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
            return [
                'sucursal'=>$this->sucursal->nombre,
                'caja'=>$this->descripcion,
                'cantidad'=>$this->cantidad,
                'estado'=>$this->estado,
                'detalle'=> ["route"=>'/movimiento/'],
                'id'=>$this->id
            ];

    }
}
