<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StockProductosResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"=>$this->id,
          "nombre"=>$this->producto->nombre,
          "cantidad"=>$this->cantidad,
            "descripcion"=>$this->producto->descripcion
        ];
    }
}
