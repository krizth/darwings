<?php

namespace App\Http\Resources;

use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Http;
use Nyholm\Psr7\Request;
use Illuminate\Http\Response as iHttp;
use Ramsey\Collection\Collection;

class MainResponse
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $requestData
     * @param JsonResource $requestType
     * @param array $status
     * @return void
     */
    public  static function normalizeWithResource(Request $requestData,JsonResource $requestType, $status=["text"=>"OK","code"=>iHttp::HTTP_OK])
    {
        $requestType::collection( [
            "code"=>$status["code"],
            "status"=>$status["text"],
            "data"=>$requestData
        ]);
    }

    public static function normalize($request, $status=["text"=>"OK","code"=>iHttp::HTTP_OK]){
        return  [
            "code"=>$status["code"],
            "status"=>$status["text"],
            "content"=>$request
        ];
    }
}
