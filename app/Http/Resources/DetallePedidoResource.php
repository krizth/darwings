<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DetallePedidoResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"=>$this->id,
            "id_pedido"=>$this->id_pedido,
            "producto"=>$this->productos->nombre,
            "cantidad"=>$this->cantidad,
            "descripcion"=>$this->productos->descripcion,
            "comentario"=>$this->descripcion

        ];
    }
}
