<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class MovimientosResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'motivo_movimiento'=>$this->motivoMovimiento->motivo,
            'monto'=>$this->monto,
            'usuario'=>$this->user->name,
            'comanda'=>$this->id_pedido,
            'medio_pago'=>$this->medioPago!==null?$this->medioPago["nombre"]:"",
            'fecha'=>$this->created_at->format('d/m/Y')
        ];
    }
}
