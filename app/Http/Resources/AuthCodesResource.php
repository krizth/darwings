<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AuthCodesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
          'creador'=>$this->owner["name"],
            'usuario_destino'=>$this->targetUser?$this->targetUser["name"]:"Sin Destinatario Asignado",
            'codigo'=>$this->codigo,
            'cantidad'=>$this->cantidad?$this->cantidad:"",
            'movimiento'=>$this->motivoMovimiento?$this->motivoMovimiento["motivo"]:"No se ha realizado movimiento",
            'user_uso'=>$this->userUse?$this->userUse["name"]:"Nadie ha usado este código",
            'imagen'=>$this->imagen
        ];
    }
}
