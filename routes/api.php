<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('login', 'PassportController@login');
Route::middleware('auth:api')->prefix('login')->group(function () {
    Route::get('/options', 'LoginController@postLoginOptions');
    Route::post('/verify', 'LoginController@postLoginParamsVerification');
    Route::get('/verify', 'LoginController@verify');
});
Route::post('/logout', 'LoginController@logout')->middleware("auth:api");

Route::middleware('auth:api')->group(function () {
    Route::get('user', 'PassportController@details');
});

Route::get('users/find', 'UserController@find')->name('users.find')->middleware('auth:api');
Route::resource('users', 'UserController')->middleware('auth:api');

Route::get('/products/find', 'ProductosController@find')->name('products.find')->middleware('auth:api');
Route::resource('products', 'ProductosController')->middleware('auth:api');

Route::get('clients/find', 'ClientesController@find')->name('clients.find')->middleware('auth:api');
Route::resource('clients', 'ClientesController')->middleware('auth:api');

Route::get('orders/print/{id}', 'PedidosController@imprimirPedido')->name('orders.print')->where('id', '[0-9]+')->middleware('auth:api');
Route::get('orders/find', 'PedidosController@find')->name('orders.find')->middleware('auth:api');
Route::resource('orders', 'PedidosController')->middleware('auth:api');

Route::get('orderDetail/find', 'DetallePedidoController@find')->name('orderDetail.find')->middleware('auth:api');
Route::resource('orderDetail', 'DetallePedidoController')->middleware('auth:api');

Route::get('stockProduct/find', 'StockProductosController@find')->name('stockProduct.find')->middleware('auth:api');
Route::resource('stockProduct', 'StockProductosController')->middleware('auth:api');

Route::get('caja/currency', 'CajaController@currency')->name('caja.currency')->middleware('auth:api');
Route::get('caja/informe', 'CajaController@informe')->name('caja.informe')->middleware('auth:api');
Route::resource('caja', 'CajaController')->middleware('auth:api');

Route::resource('movimiento', 'MovimientosController')->middleware('auth:api');

Route::get('sucursal/find', 'SucursalController@find')->name('sucursal.find')->middleware('auth:api');

Route::resource('sucursal', 'SucursalController')->middleware('auth:api');
Route::resource('estado-pedido', 'EstadoPedidoController')->middleware('auth:api');
Route::resource('delivery', 'DeliveryController')->middleware('auth:api');

Route::resource('discount', 'DescuentosController')->middleware('auth:api');

Route::resource('paymentMethod', 'MedioPagoController')->middleware('auth:api');

Route::resource('auth-code', 'AuthCodesController')->middleware('auth:api');
Route::get('images/{filename}','AuthCodesController@images')->middleware('auth:api');
Route::post('images/upload','AuthCodesController@upload')->middleware('auth:api');
Route::delete('images','AuthCodesController@deleteImage')->middleware('auth:api');
