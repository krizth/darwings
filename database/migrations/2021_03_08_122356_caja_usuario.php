<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CajaUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('caja_usuario', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_caja')->unsigned();
            $table->bigInteger("id_users")->unsigned();
            $table->softDeletes();
            $table->foreign("id_caja")->references("id")->on("caja");
            $table->foreign("id_users")->references("id")->on("users");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
