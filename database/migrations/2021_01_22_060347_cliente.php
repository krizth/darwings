<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Cliente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cliente', function (Blueprint $table) {
            $table->id();
            $table->string("nombre");
            $table->string("rut")->nullable()->unique();
            $table->string("direccion")->nullable();
            $table->string("telefono",13)->nullable();
            $table->tinyInteger("amigos_de_darwings")->default(0);
            $table->bigInteger('puntos')->unsigned()->default(0);
            $table->string("ciudad")->nullable();
            $table->bigInteger("pedidos")->unsigned()->nullable()->default(0);
            $table->json('location')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
