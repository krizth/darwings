<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class StockProductos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_productos', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_producto')->unsigned();
            $table->bigInteger('id_sucursal')->unsigned();
            $table->bigInteger("cantidad")->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign("id_producto")->references("id")->on("productos");
            $table->foreign("id_sucursal")->references("id")->on("sucursal");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
