<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MotivoMovimiento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('motivo_movimiento', function (Blueprint $table) {
            $table->id();
            $table->string('motivo');
            $table->string('operador',1)->nullable();
            $table->tinyInteger('req_autorizacion')->default(0)->unsigned();
            $table->softDeletes();
            $table->tinyInteger("contable")->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
