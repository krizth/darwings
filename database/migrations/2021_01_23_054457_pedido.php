<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Pedido extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedido', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_cliente')->unsigned();
            $table->string("direccion");
            $table->string("telefono")->nullable();
            $table->bigInteger('id_estado_pedido')->unsigned()->default(1);
            $table->string("comentario",300)->nullable()->default("");
            $table->bigInteger('id_sucursal')->unsigned();
            $table->json('medio_pago')->nullable();
            $table->bigInteger("id_delivery")->nullable()->unsigned();
            $table->bigInteger("precio_delivery")->nullable()->unsigned();
            $table->bigInteger('total')->unsigned();
            $table->bigInteger('subtotal')->unsigned();
            $table->bigInteger('descuento')->nullable();
            $table->tinyInteger('pagado')->default(0);
            $table->bigInteger('pago')->default(0);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign("id_sucursal")->references("id")->on("sucursal");
            $table->foreign("id_estado_pedido")->references("id")->on("estado_pedido");
            $table->foreign("id_cliente")->references("id")->on("cliente");
            $table->foreign("id_delivery")->references("id")->on("delivery");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
