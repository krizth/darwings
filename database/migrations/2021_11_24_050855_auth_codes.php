<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AuthCodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auth_codes', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_owner')->unsigned();
            $table->bigInteger('codigo')->unsigned();
            $table->bigInteger('id_user_destino')->unsigned()->nullable();
            $table->bigInteger('id_user_uso')->unsigned()->nullable();
            $table->bigInteger("cantidad")->nullable();
            $table->bigInteger('id_motivo_movimiento')->unsigned()->nullable();
            $table->string("imagen",255)->nullable();
            $table->string("comentario",255)->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign("id_user_destino")->references("id")->on("users");
            $table->foreign("id_owner")->references("id")->on("users");
            $table->foreign("id_user_uso")->references("id")->on("users");
            $table->foreign("id_motivo_movimiento")->references("id")->on("motivo_movimiento");
            //php artisan krlove:generate:model AuthCodes --table-name=auth_codes --namespace=App\\Models --output-path=Models
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
