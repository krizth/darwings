<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Movimiento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movimiento', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_caja')->unsigned();
            $table->bigInteger('id_apertura')->unsigned()->nullable();
            $table->bigInteger('id_cierre')->unsigned()->nullable();
            $table->bigInteger('id_medio_pago')->unsigned()->nullable();
            $table->bigInteger("id_usuario")->unsigned();
            $table->bigInteger("id_pedido")->unsigned()->nullable();
            $table->bigInteger("id_motivo_movimiento")->unsigned()->nullable();
            $table->bigInteger('monto');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign("id_medio_pago")->references("id")->on("medio_pago");
            $table->foreign("id_apertura")->references("id")->on("movimiento");
            $table->foreign("id_caja")->references("id")->on("caja");
            $table->foreign("id_pedido")->references("id")->on("pedido");
            $table->foreign("id_motivo_movimiento")->references("id")->on("motivo_movimiento");
            $table->foreign("id_usuario")->references("id")->on("users");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
