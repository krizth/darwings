<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Caja extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('caja', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("id_apertura")->nullable();
            $table->unsignedBigInteger("id_sucursal");
            $table->unsignedBigInteger("id_ultimo_cierre")->nullable();
            $table->bigInteger("cantidad")->nullable();
            $table->string("descripcion");
            $table->tinyInteger("estado")->default(0);
            $table->softDeletes();
            $table->timestamps();
            $table->foreign("id_sucursal")->references("id")->on("sucursal");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
