<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Productos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string("descripcion")->nullable();
            $table->bigInteger("precio")->unsigned();
            $table->bigInteger("id_descuento")->unsigned()->nullable();
            $table->bigInteger('puntos')->unsigned()->default(0);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign("id_descuento")->references("id")->on("descuento");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
