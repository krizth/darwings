<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SucursalesUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sucursales_usuario', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_sucursal')->unsigned();
            $table->bigInteger("id_usuario")->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign("id_sucursal")->references("id")->on("sucursal");
            $table->foreign("id_usuario")->references("id")->on("users");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
