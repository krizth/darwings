<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(estado_pedido::class);
        $this->call(medio_pago::class);
        $this->call(motivo_movimiento::class);
        $this->call(userSeeder::class);
        $this->call(sucursalSeeder::class);
        $this->call(cajaSeeder::class);
        $this->call(productoSeeder::class);

    }
}
