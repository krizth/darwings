<?php

namespace Database\Seeders;

use App\Models\EstadoPedido;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class estado_pedido extends Seeder
{

    public function run()
    {
        DB::table('estado_pedido')->insert([
            ["id"=>EstadoPedido::$PREPARACION,'descripcion'=>'En Preparacion'],
            ['id'=>EstadoPedido::$DESPACHADO,'descripcion'=>'Despachado'],
            ['id'=>EstadoPedido::$ENTREGADO,'descripcion'=>'Entregado'],
        ]);
    }
}
