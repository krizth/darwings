<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class sucursalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sucursal')->insert([
            ["nombre"=>"Sucursal Valdivia",'direccion'=>'Picarte 4065',"telefono"=>"+56948950090","ciudad"=>"Valdivia"],
            ['nombre'=>"Sucursal Buin",'direccion'=>'Picarte 3022',"telefono"=>"+56948950091","ciudad"=>"Buin"],
        ]);
        DB::table('sucursales_usuario')->insert([
            ["id_usuario"=>1,'id_sucursal'=>1],
            ["id_usuario"=>1,'id_sucursal'=>2],
            ["id_usuario"=>2,'id_sucursal'=>1],
            ["id_usuario"=>2,'id_sucursal'=>2],

        ]);
    }
}
