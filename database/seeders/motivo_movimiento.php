<?php

namespace Database\Seeders;

use App\Models\MotivoMovimiento;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class motivo_movimiento extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('motivo_movimiento')->insert([
            ["id"=>MotivoMovimiento::$VENTA,'motivo'=>'Venta'],
            ['id'=>MotivoMovimiento::$ENTRADA,'motivo'=>'Entrada'],
            ['id'=>MotivoMovimiento::$VUELTO,'motivo'=>'Vuelto'],
            ['id'=>MotivoMovimiento::$AJUSTE,'motivo'=>'Ajuste'],
            ['id'=>MotivoMovimiento::$APERTURA,'motivo'=>'Apertura'],
            ['id'=>MotivoMovimiento::$CIERRE,'motivo'=>'Cierre'],
            ['id'=>MotivoMovimiento::$CANCELACION,'motivo'=>'Cancelacion'],
            ['id'=>MotivoMovimiento::$DESCUENTO,'motivo'=>'Descuento'],
            ['id'=>MotivoMovimiento::$DELIVERY,'motivo'=>'Delivery'],
            ['id'=>MotivoMovimiento::$GASTO,'motivo'=>'Gasto'],
        ]);
    }
}
