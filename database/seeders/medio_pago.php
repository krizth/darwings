<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class medio_pago extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('medio_pago')->insert([
            ["id"=>1,'nombre'=>'Efectivo'],
            ['id'=>2,'nombre'=>'Tarjeta'],
            ['id'=>3,'nombre'=>'Transferencia'],
        ]);
    }
}
