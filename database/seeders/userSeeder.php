<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class userSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Admin = new \App\Models\User();
        $Admin->password = Hash::make('password');
        $Admin->email = 'b.fregni.j@gmail.com';
        $Admin->name = 'Bastian Fregni';
        $Admin->isAdmin=true;
        $Admin->save();

        $user = new \App\Models\User();
        $user->password = Hash::make('password');
        $user->email = 'krizth2307@gmail.com';
        $user->name = 'Cristobal Avila';
        $user->isAdmin=false;
        $user->save();
    }
}
