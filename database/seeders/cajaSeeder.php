<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class cajaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('caja')->insert([
            ["cantidad"=>0,'descripcion'=>'Caja 1',"estado"=>0,"id_sucursal"=>1],
            ["cantidad"=>0,'descripcion'=>'Caja 1',"estado"=>0,"id_sucursal"=>2],
        ]);
        DB::table('caja_usuario')->insert([
            ["id_users"=>1,'id_caja'=>1],
            ["id_users"=>1,'id_caja'=>2],
            ["id_users"=>2,'id_caja'=>2],
            ["id_users"=>2,'id_caja'=>1],
        ]);
    }
}
