<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class productoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('productos')->insert([
            ["nombre"=>"TABLA BIG CHARLIE",'descripcion'=>'Crujientes papitas fritas, costilla deshuesada, champiñones, queso, cebolla caramelizada y huevo frito.',"precio"=>17990],
            ["nombre"=>"D´Vegg",'descripcion'=>'Proteina de Soya - Palta - Queso - Tomate - Lechuga -  Cebolla - Champiñones + Papas Fritas.',"precio"=>4990],
            ["nombre"=>"PORCION COSTILLAS ( 300 Grs )",'descripcion'=>'Porción 6uni de Costillas.',"precio"=>4990],
        ]);

        /*DB::table('stock_productos')->insert([
           ["id_producto"=>1,"id_sucursal"=>1,"cantidad"=>30],
            ["id_producto"=>2,"id_sucursal"=>1,"cantidad"=>30],
        ]);*/
    }
}
